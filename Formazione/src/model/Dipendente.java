package model;

public class Dipendente extends Utente{
	private Integer id_matricola;
	private String categoria;
	private Integer livello;
	
	public Dipendente() {}

	public Integer getId_matricola() {
		return id_matricola;
	}

	public void setId_matricola(Integer id_matricola) {
		this.id_matricola = id_matricola;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public Integer getLivello() {
		return livello;
	}

	public void setLivello(Integer livello) {
		this.livello = livello;
	}

	
	
}


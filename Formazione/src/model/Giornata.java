package model;

import java.sql.Date;

public class Giornata {
	
	private int codiceGiornata;
	private int codiceEdizione;
	private Date dataGiornata;
	private String argomento;
	private int durataOre;
	
	
	
	public int getCodiceGiornata() {
		return codiceGiornata;
	}
	public void setCodiceGiornata(int codiceGiornata) {
		this.codiceGiornata = codiceGiornata;
	}
	public int getCodiceEdizione() {
		return codiceEdizione;
	}
	public void setCodiceEdizione(int codiceEdizione) {
		this.codiceEdizione = codiceEdizione;
	}
	public Date getDataGiornata() {
		return dataGiornata;
	}
	public void setDataGiornata(Date dataGiornata) {
		this.dataGiornata = dataGiornata;
	}
	public String getArgomento() {
		return argomento;
	}
	public void setArgomento(String argomento) {
		this.argomento = argomento;
	}
	public int getDurataOre() {
		return durataOre;
	}
	public void setDurataOre(int durataOre) {
		this.durataOre = durataOre;
	}
	
	
	

}

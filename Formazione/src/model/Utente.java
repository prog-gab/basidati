package model;

import java.sql.Date;

public class Utente {
	private String  CF;
	private String  cognome;
	private String  nome;
	private Date    dataN;
	private String  luogoN;
	private String  via;
	private String  citt�;
	private Integer nrCivico;
	private Integer CAP;
	private String  email;
	private String  cellulare;
	private String  noteU;
	
	
	public Utente() {}

	public String getCF() {
		return CF;
	}

	public void setCF(String cF) {
		CF = cF;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataN() {
		return dataN;
	}

	public void setDataN(Date dataN) {
		this.dataN = dataN;
	}
	
	public String getLuogoN() {
		return luogoN;
	}

	public void setLuogoN(String luogoN) {
		this.luogoN = luogoN;
	}

	public String getVia() {
		return via;
	}

	public void setVia(String via) {
		this.via = via;
	}

	public String getCitt�() {
		return citt�;
	}

	public void setCitt�(String citt�) {
		this.citt� = citt�;
	}

	public Integer getNrCivico() {
		return nrCivico;
	}

	public void setNrCivico(Integer nrCivico) {
		this.nrCivico = nrCivico;
	}

	public Integer getCAP() {
		return CAP;
	}

	public void setCAP(Integer cAP) {
		CAP = cAP;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String eMail) {
		email = eMail;
	}
	
	public String getCellulare() {
		return cellulare;
	}

	public void setCellulare(String Cell) {
		cellulare = Cell;
	}
	
	public String getNoteU() {
		return noteU;
	}

	public void setNote(String Note) {
		noteU = Note;
	}
}

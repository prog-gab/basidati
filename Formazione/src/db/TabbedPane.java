package db;

import java.awt.*;
import java.sql.*;
import java.text.*;
import javax.swing.*;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import model.*;
import net.proteanit.sql.DbUtils;

//import net.proteanit.sql.DbUtils;
import java.awt.Font;

import java.awt.event.*;

public class TabbedPane extends JFrame {

	private static final long serialVersionUID = 7979097674893931055L;
	private JTextField nome;
	private JPasswordField passwordField;
	
	  @SuppressWarnings("deprecation")
	
	  public TabbedPane(){
		  
		DBConnection dataSource = null;
        @SuppressWarnings({ "static-access", "unused" }) 
        Connection connection = (Connection) dataSource.getMsSQLConnection();
        
        JPanel firstPanel = new JPanel();
        JLabel username = new JLabel("Username /");
        username.setFont(new Font("Time", Font.BOLD,20));
        username.setBounds(200, 100, 178, 31);
        JLabel password = new JLabel("Password");
        password.setFont(new Font("Time", Font.BOLD, 20));
        password.setBounds(320, 100, 162, 42);
        firstPanel.add(username);
        firstPanel.add(password);
        
        JPanel panel = new JPanel();
        getContentPane().add(panel, BorderLayout.CENTER);
        panel.setLayout(null);
        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane.setBounds(60, 20, 1032, 850);

        panel.add(tabbedPane);
        panel.setSize(1000, 1000);
        
        nome = new JTextField();
        nome.setBounds(500, 500, 105, 35);
        firstPanel.add(nome);
        nome.setColumns(15);
        
        passwordField = new JPasswordField();
        passwordField.setBounds(100, 500, 105, 35);
        firstPanel.add(passwordField);
        passwordField.setColumns(15);

        JLabel lblNewLabel = new JLabel("");
        //Image img = new ImageIcon(this.getClass().getResource("/technogym.png")).getImage();
        //lblNewLabel.setIcon(new ImageIcon(img));
        //lblNewLabel.setBounds(0, 0, 600, 500);
        firstPanel.add(lblNewLabel);
        
        JLabel lblNewLabel_1 = new JLabel("");
        JButton login = new JButton("");
        //Image img1 = new ImageIcon(this.getClass().getResource("/ok.png")).getImage();
        //login.setIcon(new ImageIcon(img1));
        login.setFont(new Font("Yu Gothic UI Semibold", Font.PLAIN, 14));
        login.setBounds(639, 25, 86, 48);
        firstPanel.add(login);
        lblNewLabel_1.setBounds(20, 20, 20, 20);
        firstPanel.add(lblNewLabel_1);
        
       

        
        /***********************  UTENTE  ******************************/
        
        JPanel jpP = new JPanel();
        jpP.setLayout(null);
        
        JScrollPane scrollP = new JScrollPane(); //scroll tabella
        scrollP.setBounds(6, 31, 950, 330);
        jpP.add(scrollP);
        
        JTable tableU = new JTable();//crea  tabella
        tableU.setRowHeight(23);
        scrollP.setViewportView(tableU);

        
        jpP.add(tableU.getTableHeader(), BorderLayout.NORTH); //intestazione tabella
        //bottoni
        JButton loadU = new JButton("Leggi dati");
        loadU.setFont(new Font("Arial", Font.BOLD, 16));
        loadU.setBounds(530, 730, 126, 23);
        
        JButton updateU = new JButton("Aggiorna dati");
        updateU.setFont(new Font("Arial", Font.BOLD, 16));
        updateU.setBounds(350, 730, 160, 23);
        
        JButton delU = new JButton("Cancella dati");
        delU.setFont(new Font("Arial", Font.BOLD, 16));
        delU.setBounds(185, 730, 145, 23);
        
        JButton insertU = new JButton("Inserisci utente");
        insertU.setFont(new Font("Arial", Font.BOLD, 16));
        insertU.setBounds(16, 730, 160, 23);
        //label 
        JLabel jlWelcome = new JLabel("Compila per inserire / cancellare dati: ");
        jlWelcome.setFont(new Font("Times", Font.BOLD,14));
        jlWelcome.setBounds(100, 380, 400, 20);
        
        JLabel jlCF = new JLabel("CF");
        jlCF.setFont(new Font("Times", Font.BOLD,14));
        jlCF.setBounds(20, 410, 97, 20);
        
        JTextField jtfP = new JTextField(); //campo text
        jtfP.setBounds(140, 410, 97, 20);
        jtfP.setText("");
        //////////////////////////////////////////////////////////////////////////////////////////////
        JLabel jlCognome = new JLabel("COGNOME");
        jlCognome.setFont(new Font("Times", Font.BOLD,14));
        jlCognome.setBounds(20, 430, 97, 20);
        JTextField jtfP2 = new JTextField();
        jtfP2.setBounds(140, 430, 97, 20);
        jtfP2.setText("");
        
        JLabel jlNome = new JLabel("NOME");
        jlNome.setFont(new Font("Times", Font.BOLD,14));
        jlNome.setBounds(20, 450, 97, 20);
        JTextField jtfP1 = new JTextField();
        jtfP1.setBounds(140, 450, 97, 20);
        jtfP1.setText("");
        ///////////////////////////////////////////////////////////////////////////////////////////
        JLabel jlDataNascita = new JLabel("DATA NASCITA");
        jlDataNascita.setFont(new Font("Times", Font.BOLD,14));
        jlDataNascita.setBounds(20, 470, 97, 20);
        JTextField jtfP3 = new JTextField();
        jtfP3.setBounds(140, 470, 97, 20);
        jtfP3.setText("");
        
        JLabel jlLuogoNascita = new JLabel("LUOGO NASCITA");
        jlLuogoNascita.setFont(new Font("Times", Font.BOLD,14));
        jlLuogoNascita.setBounds(20, 490, 97, 20);
        JTextField jtfP4 = new JTextField();
        jtfP4.setBounds(140, 490, 97, 20);
        jtfP4.setText("");
        
        JLabel jlVia = new JLabel("VIA");
        jlVia.setFont(new Font("Times", Font.BOLD,14));
        jlVia.setBounds(20, 510, 97, 20);
        JTextField jtfP5 = new JTextField();
        jtfP5.setBounds(140, 510, 97, 20);
        jtfP5.setText("");
        
        JLabel jlNrCivico = new JLabel("CIVICO");
        jlNrCivico.setFont(new Font("Times", Font.BOLD,14));
        jlNrCivico.setBounds(20, 530, 97, 20);
        JTextField jtfP6 = new JTextField();
        jtfP6.setBounds(140, 530, 97, 20);
        jtfP6.setText("");
        
        JLabel jlCitta = new JLabel("CITTA'");
        jlCitta.setFont(new Font("Times", Font.BOLD,14));
        jlCitta.setBounds(20, 550, 97, 20);
        JTextField jtfP7 = new JTextField();
        jtfP7.setBounds(140, 550, 97, 20);
        jtfP7.setText("");       
        
        
        JLabel jlCAP = new JLabel("CAP");
        jlCAP.setFont(new Font("Times", Font.BOLD,14));
        jlCAP.setBounds(20, 570, 97, 20);
        JTextField jtfP8 = new JTextField();
        jtfP8.setBounds(140, 570, 97, 20);
        jtfP8.setText("");
        
        JLabel jlEmail = new JLabel("E-MAIL");
        jlEmail.setFont(new Font("Times", Font.BOLD,14));
        jlEmail.setBounds(20, 590, 97, 20);
        JTextField jtfP9 = new JTextField();
        jtfP9.setBounds(140, 590, 97, 20);
        jtfP9.setText("");
        
        JLabel jlCellulare = new JLabel("CELLULARE");
        jlCellulare.setFont(new Font("Times", Font.BOLD,14));
        jlCellulare.setBounds(20, 610, 97, 20);
        JTextField jtfP10 = new JTextField();
        jtfP10.setBounds(140, 610, 97, 20);
        jtfP10.setText("");
        
       
        // Bottone per pulire i campi
        JLabel jlClear = new JLabel("Click per pulire le aree di inseriemto");
        jlClear.setFont(new Font("Times", Font.BOLD,14));
        jlClear.setBounds(650, 570, 400, 20);
        
        JButton b = new JButton("PULISCI");
        b.setFont(new Font("Times", Font.BOLD,14));
        b.setBounds(700, 600, 100, 70);        
		b.addActionListener(new ActionListener(){	
			//Bottone premuto, fai la pulizia dei campi
		    public void actionPerformed(ActionEvent e){
		        jtfP.setText("");
		        jtfP1.setText("");
		        jtfP2.setText("");
		        jtfP3.setText("");
		        jtfP4.setText("");
		        jtfP5.setText("");
		        jtfP6.setText("");		        
		        jtfP7.setText("");
		        jtfP8.setText("");
		        jtfP9.setText("");
		        jtfP10.setText("");
		    }
		});
		
		jpP.add(jlClear);
        jpP.add(b); // buttore per pulire 
        jpP.add(jlCF);
        jpP.add(jlCognome);
        jpP.add(jlNome);
        jpP.add(jlDataNascita);
        jpP.add(jlLuogoNascita);
        jpP.add(jlVia);
        jpP.add(jlNrCivico);
        jpP.add(jlCitta);
        jpP.add(jlCAP);
        jpP.add(jlEmail);
        jpP.add(jlCellulare);
        
        jpP.add(jlWelcome);
        jpP.add(jtfP);
        jpP.add(jtfP1);
        jpP.add(jtfP2);
        jpP.add(jtfP3);
        jpP.add(jtfP4);
        jpP.add(jtfP5);
        jpP.add(jtfP6);
        jpP.add(jtfP7);
        jpP.add(jtfP8);
        jpP.add(jtfP9);
        jpP.add(jtfP10);
        
        
        jpP.add(insertU);
        jpP.add(delU);
        jpP.add(updateU);
        jpP.add(loadU);
        
        
        delU.addActionListener(e->{	
            //Bottone premuto, rendi invisibile i campi 
        	jtfP10.setVisible(false);
        	jtfP9.setVisible(false);
        	jtfP8.setVisible(false);
        	jtfP7.setVisible(false);
        	jtfP6.setVisible(false);
        	jtfP5.setVisible(false);
        	jtfP4.setVisible(false);
        	jtfP3.setVisible(false);
        	jtfP2.setVisible(false);
        	jtfP1.setVisible(false);
        	
        	//rendi invisibile le label
        	jlCellulare.setVisible(false);
        	jlEmail.setVisible(false);
        	jlCAP.setVisible(false);
        	jlCitta.setVisible(false);
        	jlNrCivico.setVisible(false);
        	jlVia.setVisible(false);
        	jlLuogoNascita.setVisible(false);
        	jlDataNascita.setVisible(false);
        	jlNome.setVisible(false);
        	jlCognome.setVisible(false);
        	
        	
            Tabella_Utente u = new Tabella_Utente();
            if (jtfP.getText().length()!=16 ) {
            	String message = "Errore nel inserimento dei campi ";
                JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                        JOptionPane.ERROR_MESSAGE);
            }else {
            	u.delete(jtfP.getText());
            }
            PreparedStatement statement = null;
            String query = "select * from utenti";
            
            
            
                try {
                    statement =  (PreparedStatement) connection.prepareStatement(query);  
                    ResultSet rs = statement.executeQuery();    
                    tableU.setModel(DbUtils.resultSetToTableModel(rs));// Caricamento Cancellazione dati nella Tabella
        	        tableU.getColumnModel().getColumn(0).setPreferredWidth(100);  //larghezza campo CF tabella ))      	       
        	        //tableU.getColumnModel().getColumn(6).setPreferredWidth(30);
        	        //tableU.getColumnModel().getColumn(7).setPreferredWidth(30);        	       

                  
                } catch (Exception e1) {
                    e1.printStackTrace();
                } 
        });
        
        
        loadU.addActionListener(e->{
        	PreparedStatement statement = null; 
        	String query = "select * from utenti";
        	
        		try {
        			statement = (PreparedStatement) connection.prepareStatement(query);
        			ResultSet rs = statement.executeQuery();
        			tableU.setModel(DbUtils.resultSetToTableModel(rs));
        	        tableU.getColumnModel().getColumn(0).setPreferredWidth(100);        	       
        	        //tableU.getColumnModel().getColumn(6).setPreferredWidth(30);
        	        //tableU.getColumnModel().getColumn(7).setPreferredWidth(30);        	       
        		} catch (Exception e1){
        				e1.printStackTrace();
        		} 
        });
        
        updateU.addActionListener(e->{
        	jtfP10.setVisible(true);
        	jtfP9.setVisible(true);
        	jtfP8.setVisible(true);
        	jtfP7.setVisible(true);
        	jtfP6.setVisible(true);
        	jtfP5.setVisible(true);
        	jtfP4.setVisible(true);
        	jtfP3.setVisible(true);
        	jtfP2.setVisible(true);
        	jtfP1.setVisible(true);
        	
        	jlCellulare.setVisible(true);
        	jlEmail.setVisible(true);
        	jlCAP.setVisible(true);
        	jlCitta.setVisible(true);
        	jlNrCivico.setVisible(true);
        	jlVia.setVisible(true);
        	jlLuogoNascita.setVisible(true);
        	jlDataNascita.setVisible(true);
        	jlNome.setVisible(true);
        	jlCognome.setVisible(true);
        	
        	// formato data
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            java.util.Date parsed = null; 
        	
            if (jtfP.getText().length()!=16 || check(jtfP1.getText()) || check(jtfP2.getText())  || check(jtfP3.getText()) 
            		|| check(jtfP4.getText())|| check(jtfP5.getText()) || !check(jtfP6.getText()) || check(jtfP7.getText())
            		|| !check(jtfP8.getText())|| check(jtfP9.getText())|| check(jtfP10.getText())){
                String message = "Errore nel inserimento dei campi ";
                JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                        JOptionPane.ERROR_MESSAGE);
            } 
            else {
            	try {
                    parsed = format.parse(jtfP3.getText());     
                } catch (Exception e2) {
                    // TODO Auto-generated catch block
                    e2.printStackTrace();
                }
                java.sql.Date data = new java.sql.Date(parsed.getTime());
                
            	Tabella_Utente u = new Tabella_Utente();
            	Utente ul = new Utente();
            	ul.setCF(jtfP.getText());
            	ul.setCognome(jtfP1.getText());
            	ul.setNome(jtfP2.getText());
            	ul.setDataN(data);
            	ul.setLuogoN(jtfP4.getText());            	
            	ul.setVia(jtfP5.getText());
            	ul.setNrCivico(Integer.parseInt(jtfP6.getText()));
            	ul.setCitt�(jtfP7.getText());
            	ul.setCAP(Integer.parseInt(jtfP8.getText()));
            	ul.setEmail(jtfP9.getText());
            	ul.setCellulare(jtfP10.getText());

          		u.update(ul);
   
              PreparedStatement statement = null;
              String query = "select * from utenti";
              
                  try {
                	  statement = (PreparedStatement) connection.prepareStatement(query);
          			ResultSet rs = statement.executeQuery();
          			tableU.setModel(DbUtils.resultSetToTableModel(rs));
          	        tableU.getColumnModel().getColumn(0).setPreferredWidth(100);        	       
          	        //tableC.getColumnModel().getColumn(6).setPreferredWidth(30);
          	        //tableC.getColumnModel().getColumn(7).setPreferredWidth(30); 
          		} catch ( Exception e1) {
          			e1.printStackTrace();
          		}
                  jtfP.setText("");
                  jtfP1.setText("");
                  jtfP2.setText("");
                  jtfP3.setText("");
                  jtfP4.setText("");
                  jtfP5.setText("");
                  jtfP6.setText("");		        
                  jtfP7.setText("");	
                  jtfP8.setText("");	
                  jtfP9.setText("");	
                  jtfP10.setText("");
            }
          });
        
        //Action Inserimento
        insertU.addActionListener(e->{
        	jtfP10.setVisible(true);
        	jtfP9.setVisible(true);
        	jtfP8.setVisible(true);
        	jtfP7.setVisible(true);
        	jtfP6.setVisible(true);
        	jtfP5.setVisible(true);
        	jtfP4.setVisible(true);
        	jtfP3.setVisible(true);
        	jtfP2.setVisible(true);
        	jtfP1.setVisible(true);
        	
        	jlCellulare.setVisible(true);
        	jlEmail.setVisible(true);
        	jlCAP.setVisible(true);
        	jlCitta.setVisible(true);
        	jlNrCivico.setVisible(true);
        	jlVia.setVisible(true);
        	jlLuogoNascita.setVisible(true);
        	jlDataNascita.setVisible(true);
        	jlNome.setVisible(true);
        	jlCognome.setVisible(true);
        	  
        	// formato data
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            java.util.Date parsed = null; 
            
            if (jtfP.getText().length()!=16 || check(jtfP1.getText()) || check(jtfP2.getText())  || check(jtfP3.getText()) 
            		|| check(jtfP4.getText())|| check(jtfP5.getText()) || !check(jtfP6.getText()) || check(jtfP7.getText())
            		|| !check(jtfP8.getText())|| check(jtfP9.getText())|| check(jtfP10.getText())){
                  String message = "Errore nel inserimento dei campi ";
                  JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                          JOptionPane.ERROR_MESSAGE);
            }
        	else {
        		try {
                    parsed = format.parse(jtfP3.getText());     
                } catch (Exception e2) {
                    // TODO Auto-generated catch block
                    e2.printStackTrace();
                }
                java.sql.Date data = new java.sql.Date(parsed.getTime());
        		
            	Tabella_Utente u = new Tabella_Utente();
            	Utente ul = new Utente();
            	ul.setCF(jtfP.getText());
            	ul.setCognome(jtfP1.getText());
            	ul.setNome(jtfP2.getText());
            	ul.setDataN(data);
            	ul.setLuogoN(jtfP4.getText());            	
            	ul.setVia(jtfP5.getText());
            	ul.setNrCivico(Integer.parseInt(jtfP6.getText()));
            	ul.setCitt�(jtfP7.getText());
            	ul.setCAP(Integer.parseInt(jtfP8.getText()));
            	ul.setEmail(jtfP9.getText());
            	ul.setCellulare(jtfP10.getText());
            	
            	u.persist(ul);
            	
            	PreparedStatement statement = null;
            	String query = "select * from utenti";
            	
            		try {
            			statement = (PreparedStatement) connection.prepareStatement(query);
            			ResultSet rs = statement.executeQuery();
            			tableU.setModel(DbUtils.resultSetToTableModel(rs));
            	        tableU.getColumnModel().getColumn(0).setPreferredWidth(100);        	       
            	        //tableC.getColumnModel().getColumn(6).setPreferredWidth(30);
            	        //tableC.getColumnModel().getColumn(7).setPreferredWidth(30); 
            		} catch ( Exception e1) {
            			e1.printStackTrace();
            		}
    		     jtfP.setText("");
    		     jtfP1.setText("");
    		     jtfP2.setText("");
    		     jtfP3.setText("");
    		     jtfP4.setText("");
    		     jtfP5.setText("");
    		     jtfP6.setText("");		        
    		     jtfP7.setText("");	
    		     jtfP8.setText("");	
    		     jtfP9.setText("");	
    		     jtfP10.setText("");	
        	  } 
           });

/**********************************************************************************************************************/

    /***********************  MODALITA' FORMATIVA  ******************************/
        
        JPanel jpMF = new JPanel();
        jpMF.setLayout(null);
        
        JScrollPane scrollMF = new JScrollPane(); //scroll tabella
        scrollMF.setBounds(6, 31, 950, 330);
        jpMF.add(scrollMF);
        
        JTable tableMF = new JTable();//crea  tabella
        tableMF.setRowHeight(23);
        scrollMF.setViewportView(tableMF);

        
        jpMF.add(tableMF.getTableHeader(), BorderLayout.NORTH); //intestazione tabella
        //bottoni
        JButton loadMF = new JButton("Leggi dati");
        loadMF.setFont(new Font("Arial", Font.BOLD, 16));
        loadMF.setBounds(530, 730, 126, 23);
        
        JButton updateMF = new JButton("Aggiorna dati");
        updateMF.setFont(new Font("Arial", Font.BOLD, 16));
        updateMF.setBounds(350, 730, 160, 23);
        
        JButton delMF = new JButton("Cancella dati");
        delMF.setFont(new Font("Arial", Font.BOLD, 16));
        delMF.setBounds(185, 730, 145, 23);
        
        JButton insertMF = new JButton("Inserisci modalita");
        insertMF.setFont(new Font("Arial", Font.BOLD, 16));
        insertMF.setBounds(16, 730, 160, 23);
        //label 
        JLabel jlWelcome1 = new JLabel("Compila per inserire / cancellare dati: ");
        jlWelcome1.setFont(new Font("Times", Font.BOLD,14));
        jlWelcome1.setBounds(100, 380, 400, 20);
        
        JLabel jlID_MF = new JLabel("ID_MODALITA");
        jlID_MF.setFont(new Font("Times", Font.BOLD,14));
        jlID_MF.setBounds(20, 410, 97, 20);
        
        JTextField jtfMF = new JTextField(); //campo text
        jtfMF.setBounds(140, 410, 97, 20);
        jtfMF.setText("");
        
        JLabel jlTipologia = new JLabel("TIPOLOGIA");
        jlTipologia.setFont(new Font("Times", Font.BOLD,14));
        jlTipologia.setBounds(20, 430, 97, 20);
        JTextField jtfMF1 = new JTextField();
        jtfMF1.setBounds(140, 430, 97, 20);
        jtfMF1.setText("");
        
              
       
        // Bottone per pulire i campi
        JLabel jlClear1 = new JLabel("Click per pulire le aree di inseriemto");
        jlClear1.setFont(new Font("Times", Font.BOLD,14));
        jlClear1.setBounds(650, 570, 400, 20);
        
        JButton b1 = new JButton("PULISCI");
        b1.setFont(new Font("Times", Font.BOLD,14));
        b1.setBounds(700, 600, 100, 70);        
		b1.addActionListener(new ActionListener(){	
			//Bottone premuto, fai la pulizia dei campi
		    public void actionPerformed(ActionEvent e){
		        jtfMF.setText("");
		        jtfMF1.setText("");
		        
		    }
		});
		
		jpMF.add(jlClear1);
        jpMF.add(b1); // buttore per pulire 
        jpMF.add(jlID_MF);
        jpMF.add(jlTipologia);
        
        
        jpMF.add(jlWelcome1);
        jpMF.add(jtfMF);
        jpMF.add(jtfMF1);
        
                
        jpMF.add(insertMF);
        jpMF.add(delMF);
        jpMF.add(updateMF);
        jpMF.add(loadMF);
        
        
        delMF.addActionListener(e->{	
            //Bottone premuto, rendi invisibile i campi 
        	jtfMF1.setVisible(false);
        	//jtfMF.setVisible(false);
        	
        	
        	//rendi invisibile le label
        	jlTipologia.setVisible(false);
        	        	
        	
            Tabella_ModalitaF mf = new Tabella_ModalitaF ();
            if (jtfMF.getText().length()<1 ) {
            	String message = "Errore nel inserimento dei campi ";
                JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                        JOptionPane.ERROR_MESSAGE);
            }else {
            	mf.delete(Integer.parseInt(jtfMF.getText())); // per  il metodo delete convertire la stringa  in un intero
            }
            PreparedStatement statement = null;
            String query = "select * from modalita_form";
            
            
            
                try {
                    statement =  (PreparedStatement) connection.prepareStatement(query);  
                    ResultSet rs = statement.executeQuery();    
                    tableMF.setModel(DbUtils.resultSetToTableModel(rs));// Caricamento Cancellazione dati nella Tabella
                    tableMF.getColumnModel().getColumn(0).setPreferredWidth(100);  //larghezza campo CF tabella ))      	       
        	        //tableU.getColumnModel().getColumn(6).setPreferredWidth(30);
        	        //tableU.getColumnModel().getColumn(7).setPreferredWidth(30);        	       

                  
                } catch (Exception e1) {
                    e1.printStackTrace();
                } 
        });
        
        //Lettura tabella
        loadMF.addActionListener(e->{
        	PreparedStatement statement = null; 
        	String query = "select * from modalita_form";
        	
        		try {
        			statement = (PreparedStatement) connection.prepareStatement(query);
        			ResultSet rs = statement.executeQuery();
        			tableMF.setModel(DbUtils.resultSetToTableModel(rs));
        	        tableMF.getColumnModel().getColumn(0).setPreferredWidth(100);        	       
        	        //tableU.getColumnModel().getColumn(6).setPreferredWidth(30);
        	        //tableU.getColumnModel().getColumn(7).setPreferredWidth(30);        	       
        		} catch (Exception e1){
        				e1.printStackTrace();
        		} 
        });
        
        updateMF.addActionListener(e->{
        	jlID_MF.setVisible(true);
        	jtfMF.setVisible(true);
        	jtfMF1.setVisible(true);
        	jlTipologia.setVisible(true);
        	
            if (jtfMF.getText().length()<1 || !check(jtfMF.getText()) || check(jtfMF1.getText())){
                String message = "Errore nel inserimento dei campi ";
                JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                        JOptionPane.ERROR_MESSAGE);
            } 
            else {            
            	Tabella_ModalitaF mf = new Tabella_ModalitaF();
            	ModalitaF mfl = new ModalitaF();
            	mfl.setId_modalita(Integer.parseInt(jtfMF.getText()));
            	mfl.setTipologia(jtfMF1.getText());
            	
          		mf.update(mfl);
   
              PreparedStatement statement = null;
              String query = "select * from modalita_form";
              
                  try {
                	statement = (PreparedStatement) connection.prepareStatement(query);
          			ResultSet rs = statement.executeQuery();
          			tableMF.setModel(DbUtils.resultSetToTableModel(rs));
          	        tableMF.getColumnModel().getColumn(0).setPreferredWidth(100);        	       
          	        //tableC.getColumnModel().getColumn(6).setPreferredWidth(30);
          	        //tableC.getColumnModel().getColumn(7).setPreferredWidth(30); 
          		} catch ( Exception e1) {
          			e1.printStackTrace();
          		}
                  jtfMF.setText("");
                  jtfMF1.setText("");
                 
            }
          });
        
        //Action Inserimento
        insertMF.addActionListener(e->{
           	jtfMF1.setVisible(true);
        	
        	jlTipologia.setVisible(true);
        	
        	/*
        	// formato data
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            java.util.Date parsed = null; */
            
        	 if (jtfMF.getText().length()<1 || !check(jtfMF.getText()) || check(jtfMF1.getText())){
                  String message = "Errore nel inserimento dei campi ";
                  JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                          JOptionPane.ERROR_MESSAGE);
            }
        	else {
        		/*try {
                    parsed = format.parse(jtfP3.getText());     
                } catch (Exception e2) {
                    // TODO Auto-generated catch block
                    e2.printStackTrace();
                }
                java.sql.Date data = new java.sql.Date(parsed.getTime());*/
        		
        		Tabella_ModalitaF mf = new Tabella_ModalitaF();
            	ModalitaF mfl = new ModalitaF();
            	mfl.setId_modalita(Integer.parseInt(jtfMF.getText()));
            	mfl.setTipologia(jtfMF1.getText());
            	            	
            	mf.persist(mfl);
            	
            	PreparedStatement statement = null;
            	String query = "select * from modalita_form";
            	
            		try {
            			statement = (PreparedStatement) connection.prepareStatement(query);
            			ResultSet rs = statement.executeQuery();
            			tableU.setModel(DbUtils.resultSetToTableModel(rs));
            	        tableU.getColumnModel().getColumn(0).setPreferredWidth(100);        	       
            	        //tableC.getColumnModel().getColumn(6).setPreferredWidth(30);
            	        //tableC.getColumnModel().getColumn(7).setPreferredWidth(30); 
            		} catch ( Exception e1) {
            			e1.printStackTrace();
            		}
    		     jtfMF.setText("");
    		     jtfMF1.setText("");
    		     
        	  } 
           });

/**********************************************************************************************************************/
        /**********************************************************************************************************************/

        /***********************  AREA FORMATIVA  ******************************/
            
            JPanel jpAF = new JPanel();
            jpAF.setLayout(null);
            
            JScrollPane scrollAF = new JScrollPane(); //scroll tabella
            scrollAF.setBounds(6, 31, 950, 330);
            jpAF.add(scrollAF);
            
            JTable tableAF = new JTable();//crea  tabella
            tableAF.setRowHeight(23);
            scrollAF.setViewportView(tableAF);

            
            jpAF.add(tableAF.getTableHeader(), BorderLayout.NORTH); //intestazione tabella
            //bottoni
            JButton loadAF = new JButton("Leggi dati");
            loadAF.setFont(new Font("Arial", Font.BOLD, 16));
            loadAF.setBounds(530, 730, 126, 23);
            
            JButton updateAF = new JButton("Aggiorna dati");
            updateAF.setFont(new Font("Arial", Font.BOLD, 16));
            updateAF.setBounds(350, 730, 160, 23);
            
            JButton delAF = new JButton("Cancella dati");
            delAF.setFont(new Font("Arial", Font.BOLD, 16));
            delAF.setBounds(185, 730, 145, 23);
            
            JButton insertAF = new JButton("Inserisci Area");
            insertAF.setFont(new Font("Arial", Font.BOLD, 16));
            insertAF.setBounds(16, 730, 160, 23);
            //label 
            JLabel jlWelcome2 = new JLabel("Compila per inserire / cancellare dati: ");
            jlWelcome2.setFont(new Font("Times", Font.BOLD,14));
            jlWelcome2.setBounds(100, 380, 400, 20);
            
            JLabel jlID_AF = new JLabel("ID_AREA");
            jlID_AF.setFont(new Font("Times", Font.BOLD,14));
            jlID_AF.setBounds(20, 410, 97, 20);
            
            JTextField jtfAF = new JTextField(); //campo text
            jtfAF.setBounds(140, 410, 97, 20);
            jtfAF.setText("");
            
            JLabel jlDen_AF = new JLabel("DEN_AREA");
            jlDen_AF.setFont(new Font("Times", Font.BOLD,14));
            jlDen_AF.setBounds(20, 430, 97, 20);
            JTextField jtfAF1 = new JTextField();
            jtfAF1.setBounds(140, 430, 97, 20);
            jtfAF1.setText("");
            
                  
           
            // Bottone per pulire i campi
            JLabel jlClear2 = new JLabel("Click per pulire le aree di inseriemto");
            jlClear2.setFont(new Font("Times", Font.BOLD,14));
            jlClear2.setBounds(650, 570, 400, 20);
            
            JButton b2 = new JButton("PULISCI");
            b2.setFont(new Font("Times", Font.BOLD,14));
            b2.setBounds(700, 600, 100, 70);        
    		b2.addActionListener(new ActionListener(){	
    			//Bottone premuto, fai la pulizia dei campi
    		    public void actionPerformed(ActionEvent e){
    		        jtfAF.setText("");
    		        jtfAF1.setText("");
    		        
    		    }
    		});
    		
    		jpAF.add(jlClear2);
            jpAF.add(b2); // buttore per pulire 
            jpAF.add(jlID_AF);
            jpAF.add(jlDen_AF);
            
            
            jpAF.add(jlWelcome2);
            jpAF.add(jtfAF);
            jpAF.add(jtfAF1);
            
                    
            jpAF.add(insertAF);
            jpAF.add(delAF);
            jpAF.add(updateAF);
            jpAF.add(loadAF);
            
            
            delAF.addActionListener(e->{	
                //Bottone premuto, rendi invisibile i campi 
            	jtfAF1.setVisible(false);
            	//jtfMF.setVisible(false);
            	
            	
            	//rendi invisibile le label
            	jlDen_AF.setVisible(false);
            	        	
            	
                Tabella_AreaF afT = new Tabella_AreaF ();
                if (jtfAF.getText().length()<1 ) {
                	String message = "Errore nel inserimento dei campi ";
                    JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                            JOptionPane.ERROR_MESSAGE);
                }else {
                	afT.delete(Integer.parseInt(jtfAF.getText())); // per  il metodo delete convertire la stringa  in un intero
                }
                PreparedStatement statement = null;
                String query = "select * from area_form";
                
                
                
                    try {
                        statement =  (PreparedStatement) connection.prepareStatement(query);  
                        ResultSet rs = statement.executeQuery();    
                        tableAF.setModel(DbUtils.resultSetToTableModel(rs));// Caricamento Cancellazione dati nella Tabella
                        tableAF.getColumnModel().getColumn(0).setPreferredWidth(100);  //larghezza campo CF tabella ))      	       
            	        //tableU.getColumnModel().getColumn(6).setPreferredWidth(30);
            	        //tableU.getColumnModel().getColumn(7).setPreferredWidth(30);        	       

                      
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    } 
            });
            
            //Lettura tabella
            loadAF.addActionListener(e->{
            	PreparedStatement statement = null; 
            	String query = "select * from area_form";
            	
            		try {
            			statement = (PreparedStatement) connection.prepareStatement(query);
            			ResultSet rs = statement.executeQuery();
            			tableAF.setModel(DbUtils.resultSetToTableModel(rs));
            	        tableAF.getColumnModel().getColumn(0).setPreferredWidth(100);        	       
            	        //tableU.getColumnModel().getColumn(6).setPreferredWidth(30);
            	        //tableU.getColumnModel().getColumn(7).setPreferredWidth(30);        	       
            		} catch (Exception e1){
            				e1.printStackTrace();
            		} 
            });
            
            updateAF.addActionListener(e->{
            	jlID_AF.setVisible(true);
            	jtfAF.setVisible(true);
            	jtfAF1.setVisible(true);
            	jlDen_AF.setVisible(true);
            	
                if (jtfAF.getText().length()<1 || !check(jtfAF.getText()) || check(jtfAF1.getText())){
                    String message = "Errore nel inserimento dei campi ";
                    JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                            JOptionPane.ERROR_MESSAGE);
                } 
                else {            
                	Tabella_AreaF afT = new Tabella_AreaF();
                	AreaF afl = new AreaF();
                	afl.setId_area(Integer.parseInt(jtfAF.getText()));
                	afl.setDen_area(jtfAF1.getText());
                	
              		afT.update(afl);
       
                  PreparedStatement statement = null;
                  String query = "select * from area_form";
                  
                      try {
                    	statement = (PreparedStatement) connection.prepareStatement(query);
              			ResultSet rs = statement.executeQuery();
              			tableAF.setModel(DbUtils.resultSetToTableModel(rs));
              	        tableAF.getColumnModel().getColumn(0).setPreferredWidth(100);        	       
              	        //tableC.getColumnModel().getColumn(6).setPreferredWidth(30);
              	        //tableC.getColumnModel().getColumn(7).setPreferredWidth(30); 
              		} catch ( Exception e1) {
              			e1.printStackTrace();
              		}
                      jtfAF.setText("");
                      jtfAF1.setText("");
                     
                }
              });
            
            //Action Inserimento
            insertAF.addActionListener(e->{
               	jtfAF1.setVisible(true);
            	
               	jlDen_AF.setVisible(true);
            	
            	/*
            	// formato data
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                java.util.Date parsed = null; */
                
            	 if (jtfAF.getText().length()<1 || !check(jtfAF.getText()) || check(jtfAF1.getText())){
                      String message = "Errore nel inserimento dei campi ";
                      JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                              JOptionPane.ERROR_MESSAGE);
                }
            	else {
            		/*try {
                        parsed = format.parse(jtfP3.getText());     
                    } catch (Exception e2) {
                        // TODO Auto-generated catch block
                        e2.printStackTrace();
                    }
                    java.sql.Date data = new java.sql.Date(parsed.getTime());*/
            		
            		Tabella_AreaF af = new Tabella_AreaF();
                	AreaF afl = new AreaF();
                	afl.setId_area(Integer.parseInt(jtfAF.getText()));
                	afl.setDen_area(jtfAF1.getText());
                	            	
                	af.persist(afl);
                	
                	PreparedStatement statement = null;
                	String query = "select * from area_form";
                	
                		try {
                			statement = (PreparedStatement) connection.prepareStatement(query);
                			ResultSet rs = statement.executeQuery();
                			tableAF.setModel(DbUtils.resultSetToTableModel(rs));
                	        tableAF.getColumnModel().getColumn(0).setPreferredWidth(100);        	       
                	        //tableC.getColumnModel().getColumn(6).setPreferredWidth(30);
                	        //tableC.getColumnModel().getColumn(7).setPreferredWidth(30); 
                		} catch ( Exception e1) {
                			e1.printStackTrace();
                		}
        		     jtfAF.setText("");
        		     jtfAF1.setText("");
        		     
            	  } 
               });

    /**********************************************************************************************************************/
            
            /**********************************************************************************************************************/

            /***********************  CORSO FORMATIVO  ******************************/
                
                JPanel jpCOF = new JPanel();
                jpCOF.setLayout(null);
                
                JScrollPane scrollCOF = new JScrollPane(); //scroll tabella
                scrollCOF.setBounds(6, 31, 950, 330);
                jpCOF.add(scrollCOF);
                
                JTable tableCOF = new JTable();//crea  tabella
                tableCOF.setRowHeight(23);
                scrollCOF.setViewportView(tableCOF);

                
                jpCOF.add(tableCOF.getTableHeader(), BorderLayout.NORTH); //intestazione tabella
                //bottoni
                JButton loadCOF = new JButton("Leggi dati");
                loadCOF.setFont(new Font("Arial", Font.BOLD, 16));
                loadCOF.setBounds(530, 730, 126, 23);
                
                JButton updateCOF = new JButton("Aggiorna dati");
                updateCOF.setFont(new Font("Arial", Font.BOLD, 16));
                updateCOF.setBounds(350, 730, 160, 23);
                
                JButton delCOF = new JButton("Cancella dati");
                delCOF.setFont(new Font("Arial", Font.BOLD, 16));
                delCOF.setBounds(185, 730, 145, 23);
                
                JButton insertCOF = new JButton("Inserisci Corso");
                insertCOF.setFont(new Font("Arial", Font.BOLD, 16));
                insertCOF.setBounds(16, 730, 160, 23);
                //label 
                JLabel jlWelcome3 = new JLabel("Compila per inserire / cancellare dati: ");
                jlWelcome3.setFont(new Font("Times", Font.BOLD,14));
                jlWelcome3.setBounds(100, 380, 400, 20);
                
                JLabel jlID_COF = new JLabel("ID_CORSO");
                jlID_COF.setFont(new Font("Times", Font.BOLD,14));
                jlID_COF.setBounds(20, 410, 97, 20);
                
                JTextField jtfCOF = new JTextField(); //campo text
                jtfCOF.setBounds(140, 410, 97, 20);
                jtfCOF.setText("");
                
                JLabel jlTit_COF = new JLabel("TITOLO");
                jlTit_COF.setFont(new Font("Times", Font.BOLD,14));
                jlTit_COF.setBounds(20, 430, 97, 20);
                JTextField jtfCOF1 = new JTextField();
                jtfCOF1.setBounds(140, 430, 97, 20);
                jtfCOF1.setText("");
                
                JLabel jlPres_COF = new JLabel("PRESENTAZIONE");
                jlPres_COF.setFont(new Font("Times", Font.BOLD,14));
                jlPres_COF.setBounds(20, 450, 97, 20);
                JTextField jtfCOF2 = new JTextField();
                jtfCOF2.setBounds(140, 450, 97, 20);
                jtfCOF2.setText("");
                
                                     
                // Bottone per pulire i campi
                JLabel jlClear3 = new JLabel("Click per pulire le aree di inseriemto");
                jlClear3.setFont(new Font("Times", Font.BOLD,14));
                jlClear3.setBounds(650, 570, 400, 20);
                
                JButton b3 = new JButton("PULISCI");
                b3.setFont(new Font("Times", Font.BOLD,14));
                b3.setBounds(700, 600, 100, 70);        
        		b3.addActionListener(new ActionListener(){	
        			//Bottone premuto, fai la pulizia dei campi
        		    public void actionPerformed(ActionEvent e){
        		        jtfCOF.setText("");
        		        jtfCOF1.setText("");
        		        jtfCOF2.setText("");
        		        
        		    }
        		});
        		
        		jpCOF.add(jlClear3);
        		jpCOF.add(b3); // buttore per pulire 
        		jpCOF.add(jlID_COF);
        		jpCOF.add(jlTit_COF);
        		jpCOF.add(jlPres_COF);
                
                
                jpCOF.add(jlWelcome3);
                jpCOF.add(jtfCOF);
                jpCOF.add(jtfCOF1);
                jpCOF.add(jtfCOF2);

                        
                jpCOF.add(insertCOF);
                jpCOF.add(delCOF);
                jpCOF.add(updateCOF);
                jpCOF.add(loadCOF);
                
                
                delCOF.addActionListener(e->{	
                    //Bottone premuto, rendi invisibile i campi 
                	jtfCOF1.setVisible(false);
                	jtfCOF2.setVisible(false);
                	               	
                  	//rendi invisibile le label
                	jlTit_COF.setVisible(false);
                	jlPres_COF.setVisible(false);
                	
                    Tabella_Corso cofT = new Tabella_Corso ();
                    if (jtfCOF.getText().length()<1 ) {
                    	String message = "Errore nel inserimento dei campi ";
                        JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                                JOptionPane.ERROR_MESSAGE);
                    }else {
                    	cofT.delete(Integer.parseInt(jtfCOF.getText())); // per  il metodo delete convertire la stringa  in un intero
                    }
                    PreparedStatement statement = null;
                    String query = "select * from corso";
                    
                    
                    
                        try {
                            statement =  (PreparedStatement) connection.prepareStatement(query);  
                            ResultSet rs = statement.executeQuery();    
                            tableAF.setModel(DbUtils.resultSetToTableModel(rs));// Caricamento Cancellazione dati nella Tabella
                            tableAF.getColumnModel().getColumn(0).setPreferredWidth(100);  //larghezza campo CF tabella ))      	       
                	        //tableU.getColumnModel().getColumn(6).setPreferredWidth(30);
                	        //tableU.getColumnModel().getColumn(7).setPreferredWidth(30);        	       

                          
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        } 
                });
                
                //Lettura tabella
                loadCOF.addActionListener(e->{
                	PreparedStatement statement = null; 
                	String query = "select * from corso";
                	
                		try {
                			statement = (PreparedStatement) connection.prepareStatement(query);
                			ResultSet rs = statement.executeQuery();
                			tableCOF.setModel(DbUtils.resultSetToTableModel(rs));
                	        tableCOF.getColumnModel().getColumn(0).setPreferredWidth(100);        	       
                	        //tableU.getColumnModel().getColumn(6).setPreferredWidth(30);
                	        //tableU.getColumnModel().getColumn(7).setPreferredWidth(30);        	       
                		} catch (Exception e1){
                				e1.printStackTrace();
                		} 
                });
                
                updateCOF.addActionListener(e->{
                	
                	jtfCOF.setVisible(true);
                	jlID_COF.setVisible(true);
                	jtfCOF1.setVisible(true);
                	jlTit_COF.setVisible(true);
                	jtfCOF2.setVisible(true);
                  	jlPres_COF.setVisible(true);
                	
                	
                    if (jtfCOF.getText().length()<1 || !check(jtfCOF.getText()) || check(jtfCOF1.getText())){
                        String message = "Errore nel inserimento dei campi ";
                        JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                                JOptionPane.ERROR_MESSAGE);
                    } 
                    else {            
                    	Tabella_Corso cofT = new Tabella_Corso();
                    	Corso cofl = new Corso();
                    	cofl.setId_corso(Integer.parseInt(jtfCOF.getText()));
                    	cofl.setTitolo(jtfCOF1.getText());
                    	cofl.setPresent(jtfCOF2.getText());
                    	
                  		cofT.update(cofl);
           
                      PreparedStatement statement = null;
                      String query = "select * from corso";
                      
                          try {
                        	statement = (PreparedStatement) connection.prepareStatement(query);
                  			ResultSet rs = statement.executeQuery();
                  			tableCOF.setModel(DbUtils.resultSetToTableModel(rs));
                  	        tableCOF.getColumnModel().getColumn(0).setPreferredWidth(100);        	       
                  	        //tableC.getColumnModel().getColumn(6).setPreferredWidth(30);
                  	        //tableC.getColumnModel().getColumn(7).setPreferredWidth(30); 
                  		} catch ( Exception e1) {
                  			e1.printStackTrace();
                  		}
                          jtfCOF.setText("");
                          jtfCOF1.setText("");
                          jtfCOF2.setText("");
                         
                    }
                  });
                
                //Action Inserimento
                insertCOF.addActionListener(e->{
                   	jtfCOF1.setVisible(true);
                	
                   	jlTit_COF.setVisible(true);
                   	
                	jtfCOF2.setVisible(true);
                	
                	jlPres_COF.setVisible(true);
                	
                	/*
                	// formato data
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    java.util.Date parsed = null; */
                    
                	 if (jtfCOF.getText().length()<1 || !check(jtfCOF.getText()) || check(jtfCOF1.getText())){
                          String message = "Errore nel inserimento dei campi ";
                          JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                                  JOptionPane.ERROR_MESSAGE);
                    }
                	else {
                		/*try {
                            parsed = format.parse(jtfP3.getText());     
                        } catch (Exception e2) {
                            // TODO Auto-generated catch block
                            e2.printStackTrace();
                        }
                        java.sql.Date data = new java.sql.Date(parsed.getTime());*/
                		
                		Tabella_Corso cofT = new Tabella_Corso();
                    	Corso cofl = new Corso();
                    	cofl.setId_corso(Integer.parseInt(jtfCOF.getText()));
                    	cofl.setTitolo(jtfCOF1.getText());
                    	cofl.setPresent(jtfCOF2.getText());
                    	            	
                    	cofT.persist(cofl);
                    	
                    	PreparedStatement statement = null;
                    	String query = "select * from corso";
                    	
                    		try {
                    			statement = (PreparedStatement) connection.prepareStatement(query);
                    			ResultSet rs = statement.executeQuery();
                    			tableCOF.setModel(DbUtils.resultSetToTableModel(rs));
                    	        tableCOF.getColumnModel().getColumn(0).setPreferredWidth(100);        	       
                    	        //tableC.getColumnModel().getColumn(6).setPreferredWidth(30);
                    	        //tableC.getColumnModel().getColumn(7).setPreferredWidth(30); 
                    		} catch ( Exception e1) {
                    			e1.printStackTrace();
                    		}
            		     jtfCOF.setText("");
            		     jtfCOF1.setText("");
            		     jtfCOF2.setText("");
            		     
                	  } 
                   });
                
                
                /**********************************************************************************************************************/

                /***********************  Ciclo Titolo Professionale  ******************************/
                    
                    JPanel jpCTP = new JPanel();
                    jpCTP.setLayout(null);
                    
                    JScrollPane scrollCTP = new JScrollPane(); //scroll tabella
                    scrollCTP.setBounds(6, 31, 950, 330);
                    jpCTP.add(scrollCTP);
                    
                    JTable tableCTP = new JTable();//crea  tabella
                    tableCTP.setRowHeight(23);
                    scrollCTP.setViewportView(tableCTP);

                    
                    jpCTP.add(tableCTP.getTableHeader(), BorderLayout.NORTH); //intestazione tabella
                    //bottoni
                    JButton loadCTP = new JButton("Leggi dati");
                    loadCTP.setFont(new Font("Arial", Font.BOLD, 16));
                    loadCTP.setBounds(530, 730, 126, 23);
                    
                    JButton updateCTP = new JButton("Aggiorna dati");
                    updateCTP.setFont(new Font("Arial", Font.BOLD, 16));
                    updateCTP.setBounds(350, 730, 160, 23);
                    
                    JButton delCTP = new JButton("Cancella dati");
                    delCTP.setFont(new Font("Arial", Font.BOLD, 16));
                    delCTP.setBounds(185, 730, 145, 23);
                    
                    JButton insertCTP = new JButton("Inserisci Ciclo");
                    insertCTP.setFont(new Font("Arial", Font.BOLD, 16));
                    insertCTP.setBounds(16, 730, 160, 23);
                    //label 
                    JLabel jlWelcome4 = new JLabel("Compila per inserire / cancellare dati: ");
                    jlWelcome4.setFont(new Font("Times", Font.BOLD,14));
                    jlWelcome4.setBounds(100, 380, 400, 20);
                    
                    JLabel jlID_CTP = new JLabel("ID_CICLO");
                    jlID_CTP.setFont(new Font("Times", Font.BOLD,14));
                    jlID_CTP.setBounds(20, 410, 97, 20);
                    JTextField jtfCTP = new JTextField(); //campo text
                    jtfCTP.setBounds(140, 410, 97, 20);
                    jtfCTP.setText("");
                    
                    JLabel jlDen_CTP = new JLabel("DEN_CICLO");
                    jlDen_CTP.setFont(new Font("Times", Font.BOLD,14));
                    jlDen_CTP.setBounds(20, 430, 97, 20);
                    JTextField jtfCTP1 = new JTextField();
                    jtfCTP1.setBounds(140, 430, 97, 20);
                    jtfCTP1.setText("");
                    
                    JLabel jlDesc_CTP = new JLabel("DESCRIZIONE");
                    jlDesc_CTP.setFont(new Font("Times", Font.BOLD,14));
                    jlDesc_CTP.setBounds(20, 450, 97, 20);
                    JTextField jtfCTP2 = new JTextField();
                    jtfCTP2.setBounds(140, 450, 97, 20);
                    jtfCTP2.setText("");
                    
                                         
                    // Bottone per pulire i campi
                    JLabel jlClear4 = new JLabel("Click per pulire le aree di inseriemto");
                    jlClear4.setFont(new Font("Times", Font.BOLD,14));
                    jlClear4.setBounds(650, 570, 400, 20);
                    
                    JButton b4 = new JButton("PULISCI");
                    b4.setFont(new Font("Times", Font.BOLD,14));
                    b4.setBounds(700, 600, 100, 70);        
            		b4.addActionListener(new ActionListener(){	
            			//Bottone premuto, fai la pulizia dei campi
            		    public void actionPerformed(ActionEvent e){
            		        jtfCTP.setText("");
            		        jtfCTP1.setText("");
            		        jtfCTP2.setText("");
            		        
            		    }
            		});
            		
            		jpCTP.add(jlClear4);
            		jpCTP.add(b4); // buttore per pulire 
            		jpCTP.add(jlID_CTP);
            		jpCTP.add(jlDen_CTP);
            		jpCTP.add(jlDesc_CTP);
                    
                    
                    jpCTP.add(jlWelcome4);
                    jpCTP.add(jtfCTP);
                    jpCTP.add(jtfCTP1);
                    jpCTP.add(jtfCTP2);

                            
                    jpCTP.add(insertCTP);
                    jpCTP.add(delCTP);
                    jpCTP.add(updateCTP);
                    jpCTP.add(loadCTP);
                    
                    
                    delCTP.addActionListener(e->{	
                        //Bottone premuto, rendi invisibile i campi 
                    	jtfCTP1.setVisible(false);
                    	jtfCTP2.setVisible(false);
                    	               	
                      	//rendi invisibile le label
                    	jlDen_CTP.setVisible(false);
                    	jlDesc_CTP.setVisible(false);
                    	
                        Tabella_Ciclo cicT = new Tabella_Ciclo ();
                        if (jtfCTP.getText().length()<1 ) {
                        	String message = "Errore nel inserimento dei campi ";
                            JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                                    JOptionPane.ERROR_MESSAGE);
                        }else {
                        	cicT.delete(Integer.parseInt(jtfCTP.getText())); // per  il metodo delete convertire la stringa  in un intero
                        }
                        PreparedStatement statement = null;
                        String query = "select * from ciclo";
                        
                        
                        
                            try {
                                statement =  (PreparedStatement) connection.prepareStatement(query);  
                                ResultSet rs = statement.executeQuery();    
                                tableCTP.setModel(DbUtils.resultSetToTableModel(rs));// Caricamento Cancellazione dati nella Tabella
                                tableCTP.getColumnModel().getColumn(0).setPreferredWidth(100);  //larghezza campo CF tabella ))      	       
                    	        //tableU.getColumnModel().getColumn(6).setPreferredWidth(30);
                    	        //tableU.getColumnModel().getColumn(7).setPreferredWidth(30);        	       

                              
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            } 
                    });
                    
                    //Lettura tabella
                    loadCTP.addActionListener(e->{
                    	PreparedStatement statement = null; 
                    	String query = "select * from ciclo";
                    	
                    		try {
                    			statement = (PreparedStatement) connection.prepareStatement(query);
                    			ResultSet rs = statement.executeQuery();
                    			tableCTP.setModel(DbUtils.resultSetToTableModel(rs));
                    	        tableCTP.getColumnModel().getColumn(0).setPreferredWidth(100);        	       
                    	        //tableU.getColumnModel().getColumn(6).setPreferredWidth(30);
                    	        //tableU.getColumnModel().getColumn(7).setPreferredWidth(30);        	       
                    		} catch (Exception e1){
                    				e1.printStackTrace();
                    		} 
                    });
                    
                    updateCTP.addActionListener(e->{
                    	
                    	jtfCTP.setVisible(true);
                    	jlID_CTP.setVisible(true);
                    	jtfCTP1.setVisible(true);
                    	jlDen_CTP.setVisible(true);
                    	jtfCTP2.setVisible(true);
                      	jlDesc_CTP.setVisible(true);
                    	
                    	
                        if (jtfCTP.getText().length()<1 || !check(jtfCTP.getText()) || check(jtfCTP1.getText())){
                            String message = "Errore nel inserimento dei campi ";
                            JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                                    JOptionPane.ERROR_MESSAGE);
                        } 
                        else {            
                        	Tabella_Ciclo cicT = new Tabella_Ciclo();
                        	Ciclo ctpl = new Ciclo();
                        	ctpl.setId_Ciclo(Integer.parseInt(jtfCTP.getText()));
                        	ctpl.setDen_Ciclo(jtfCTP1.getText());
                        	ctpl.setDescr(jtfCTP2.getText());
                        	
                      		cicT.update(ctpl);
               
                          PreparedStatement statement = null;
                          String query = "select * from ciclo";
                          
                              try {
                            	statement = (PreparedStatement) connection.prepareStatement(query);
                      			ResultSet rs = statement.executeQuery();
                      			tableCTP.setModel(DbUtils.resultSetToTableModel(rs));
                      	        tableCTP.getColumnModel().getColumn(0).setPreferredWidth(100);        	       
                      	        //tableC.getColumnModel().getColumn(6).setPreferredWidth(30);
                      	        //tableC.getColumnModel().getColumn(7).setPreferredWidth(30); 
                      		} catch ( Exception e1) {
                      			e1.printStackTrace();
                      		}
                              jtfCTP.setText("");
                              jtfCTP1.setText("");
                              jtfCTP2.setText("");
                             
                        }
                      });
                    
                    //Action Inserimento
                    insertCTP.addActionListener(e->{
                    	jtfCTP.setVisible(true);
                    	jlID_CTP.setVisible(true);
                    	jtfCTP1.setVisible(true);
                    	jlDen_CTP.setVisible(true);
                    	jtfCTP2.setVisible(true);
                      	jlDesc_CTP.setVisible(true);;
                    	
                    	/*
                    	// formato data
                        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                        java.util.Date parsed = null; */
                        
                    	 if (jtfCTP.getText().length()<1 || !check(jtfCTP.getText()) || check(jtfCTP1.getText())){
                              String message = "Errore nel inserimento dei campi ";
                              JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                                      JOptionPane.ERROR_MESSAGE);
                        }
                    	else {
                    		/*try {
                                parsed = format.parse(jtfP3.getText());     
                            } catch (Exception e2) {
                                // TODO Auto-generated catch block
                                e2.printStackTrace();
                            }
                            java.sql.Date data = new java.sql.Date(parsed.getTime());*/
                    		
                    		Tabella_Ciclo cicT = new Tabella_Ciclo();
                        	Ciclo ctpl = new Ciclo();
                        	ctpl.setId_Ciclo(Integer.parseInt(jtfCTP.getText()));
                        	ctpl.setDen_Ciclo(jtfCTP1.getText());
                        	ctpl.setDescr(jtfCTP2.getText());
                        	            	
                        	cicT.persist(ctpl);
                        	
                        	PreparedStatement statement = null;
                        	String query = "select * from ciclo";
                        	
                        		try {
                        			statement = (PreparedStatement) connection.prepareStatement(query);
                        			ResultSet rs = statement.executeQuery();
                        			tableCTP.setModel(DbUtils.resultSetToTableModel(rs));
                        	        tableCTP.getColumnModel().getColumn(0).setPreferredWidth(100);        	       
                        	        //tableC.getColumnModel().getColumn(6).setPreferredWidth(30);
                        	        //tableC.getColumnModel().getColumn(7).setPreferredWidth(30); 
                        		} catch ( Exception e1) {
                        			e1.printStackTrace();
                        		}
                		     jtfCTP.setText("");
                		     jtfCTP1.setText("");
                		     jtfCTP2.setText("");
                		     
                    	  } 
                       });


/*****************************************************************************************************************************************************/	
   /***********************   Titolo Professionale  ******************************/
                    
                    JPanel jpTP = new JPanel();
                    jpTP.setLayout(null);
                    
                    JScrollPane scrollTP = new JScrollPane(); //scroll tabella
                    scrollTP.setBounds(6, 31, 950, 330);
                    jpTP.add(scrollTP);
                    
                    JTable tableTP = new JTable();//crea  tabella
                    tableTP.setRowHeight(23);
                    scrollTP.setViewportView(tableTP);

                    
                    jpTP.add(tableTP.getTableHeader(), BorderLayout.NORTH); //intestazione tabella
                    //bottoni
                    JButton loadTP = new JButton("Leggi dati");
                    loadTP.setFont(new Font("Arial", Font.BOLD, 16));
                    loadTP.setBounds(530, 730, 126, 23);
                    
                    JButton updateTP = new JButton("Aggiorna dati");
                    updateTP.setFont(new Font("Arial", Font.BOLD, 16));
                    updateTP.setBounds(350, 730, 160, 23);
                    
                    JButton delTP = new JButton("Cancella dati");
                    delTP.setFont(new Font("Arial", Font.BOLD, 16));
                    delTP.setBounds(185, 730, 145, 23);
                    
                    JButton insertTP = new JButton("Inserisci Titolo ");
                    insertTP.setFont(new Font("Arial", Font.BOLD, 16));
                    insertTP.setBounds(16, 730, 160, 23);
                    //label 
                    JLabel jlWelcome5 = new JLabel("Compila per inserire / cancellare dati: ");
                    jlWelcome5.setFont(new Font("Times", Font.BOLD,14));
                    jlWelcome5.setBounds(100, 380, 400, 20);
                    
                    JLabel jlID_cTP = new JLabel("ID_CICLO");
                    jlID_cTP.setFont(new Font("Times", Font.BOLD,14));
                    jlID_cTP.setBounds(20, 410, 97, 20);
                    
                    JTextField jtfTP = new JTextField(); //campo text
                    jtfTP.setBounds(140, 410, 97, 20);
                    jtfTP.setText("");
                    
                    JLabel jlID_TP = new JLabel("ID_TITOLO");
                    jlID_TP.setFont(new Font("Times", Font.BOLD,14));
                    jlID_TP.setBounds(20, 430, 97, 20);
                    JTextField jtfTP1 = new JTextField();
                    jtfTP1.setBounds(140, 430, 97, 20);
                    jtfTP1.setText("");
                    
                    JLabel jlTit_P = new JLabel("TITOLO PROFESSIONALE");
                    jlTit_P.setFont(new Font("Times", Font.BOLD,14));
                    jlTit_P.setBounds(20, 450, 97, 20);
                    JTextField jtfTP2 = new JTextField();
                    jtfTP2.setBounds(140, 450, 97, 20);
                    jtfTP2.setText("");
                    
                                         
                    // Bottone per pulire i campi
                    JLabel jlClear5 = new JLabel("Click per pulire le aree di inseriemto");
                    jlClear5.setFont(new Font("Times", Font.BOLD,14));
                    jlClear5.setBounds(650, 570, 400, 20);
                    
                    JButton b5 = new JButton("PULISCI");
                    b5.setFont(new Font("Times", Font.BOLD,14));
                    b5.setBounds(700, 600, 100, 70);        
            		b5.addActionListener(new ActionListener(){	
            			//Bottone premuto, fai la pulizia dei campi
            		    public void actionPerformed(ActionEvent e){
            		        jtfTP.setText("");
            		        jtfTP1.setText("");
            		        jtfTP2.setText("");
            		        
            		    }
            		});
            		
            		jpTP.add(jlClear5);
            		jpTP.add(b5); // buttore per pulire 
            		jpTP.add(jlID_cTP);
            		jpTP.add(jlID_TP);
            		jpTP.add(jlTit_P);
                    
                    
            		jpTP.add(jlWelcome5);
            		jpTP.add(jtfTP);
            		jpTP.add(jtfTP1);
            		jpTP.add(jtfTP2);

                            
            		jpTP.add(insertTP);
            		jpTP.add(delTP);
            		jpTP.add(updateTP);
                    jpTP.add(loadTP);
                    
                    
                    delTP.addActionListener(e->{	
                        //Bottone premuto, rendi invisibile i campi 
                    	jtfTP.setVisible(false);
                    	jtfTP2.setVisible(false);
                    	               	
                      	//rendi invisibile le label
                    	jlID_cTP.setVisible(false);
                    	jlTit_P.setVisible(false);
                    	
                        Tabella_TitoloP titP = new Tabella_TitoloP();
                        if (jtfTP1.getText().length()<1 ) {
                        	String message = "Errore nel inserimento dei campi ";
                            JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                                    JOptionPane.ERROR_MESSAGE);
                        }else {
                        	titP.delete(Integer.parseInt(jtfTP1.getText())); // per  il metodo delete convertire la stringa  in un intero
                        }
                        PreparedStatement statement = null;
                        String query = "select * from titolo_prof";
                        
                        
                        
                            try {
                                statement =  (PreparedStatement) connection.prepareStatement(query);  
                                ResultSet rs = statement.executeQuery();    
                                tableTP.setModel(DbUtils.resultSetToTableModel(rs));// Caricamento Cancellazione dati nella Tabella
                                tableTP.getColumnModel().getColumn(0).setPreferredWidth(100);  //larghezza campo CF tabella ))      	       
                    	        //tableU.getColumnModel().getColumn(6).setPreferredWidth(30);
                    	        //tableU.getColumnModel().getColumn(7).setPreferredWidth(30);        	       

                              
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            } 
                    });
                    
                    //Lettura tabella
                    loadTP.addActionListener(e->{
                    	PreparedStatement statement = null; 
                    	String query = "select * from titolo_prof";
                    	
                    		try {
                    			statement = (PreparedStatement) connection.prepareStatement(query);
                    			ResultSet rs = statement.executeQuery();
                    			tableTP.setModel(DbUtils.resultSetToTableModel(rs));
                    	        tableTP.getColumnModel().getColumn(0).setPreferredWidth(100);        	       
                    	        //tableU.getColumnModel().getColumn(6).setPreferredWidth(30);
                    	        //tableU.getColumnModel().getColumn(7).setPreferredWidth(30);        	       
                    		} catch (Exception e1){
                    				e1.printStackTrace();
                    		} 
                    });
                    
                    updateTP.addActionListener(e->{
                    	
                    	jtfTP.setVisible(true);
                    	jlID_cTP.setVisible(true);
                    	jtfTP1.setVisible(true);
                    	jlID_TP.setVisible(true);
                    	jtfTP2.setVisible(true);
                    	jlTit_P.setVisible(true);
                    	
                    	
                        if (jtfTP.getText().length()<1 || !check(jtfTP.getText())||jtfTP1.getText().length()<1 || !check(jtfTP1.getText())){
                            String message = "Errore nel inserimento dei campi ";
                            JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                                    JOptionPane.ERROR_MESSAGE);
                        } 
                        else {            
                        	Tabella_TitoloP titP = new Tabella_TitoloP();
                        	TitoloP titpl = new TitoloP();
                        	titpl.setId_Ciclo(Integer.parseInt(jtfTP.getText()));
                        	titpl.setId_TitoloP(Integer.parseInt(jtfTP1.getText()));
                        	titpl.setNome_Tit(jtfTP2.getText());
                        	
                        	titP.update(titpl);
               
                          PreparedStatement statement = null;
                          String query = "select * from titolo_prof";
                          
                              try {
                            	statement = (PreparedStatement) connection.prepareStatement(query);
                      			ResultSet rs = statement.executeQuery();
                      			tableTP.setModel(DbUtils.resultSetToTableModel(rs));
                      	        tableTP.getColumnModel().getColumn(0).setPreferredWidth(100);        	       
                      	        //tableC.getColumnModel().getColumn(6).setPreferredWidth(30);
                      	        //tableC.getColumnModel().getColumn(7).setPreferredWidth(30); 
                      		} catch ( Exception e1) {
                      			e1.printStackTrace();
                      		}
                              jtfTP.setText("");
                              jtfTP1.setText("");
                              jtfTP2.setText("");
                             
                        }
                      });
                    
                    //Action Inserimento
                    insertTP.addActionListener(e->{
                    	jtfTP.setVisible(true);
                    	jlID_cTP.setVisible(true);
                    	jtfTP1.setVisible(true);
                    	jlID_TP.setVisible(true);
                    	jtfTP2.setVisible(true);
                    	jlTit_P.setVisible(true);
                    	
                    	/*
                    	// formato data
                        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                        java.util.Date parsed = null; */
                        
                    	 if (jtfTP.getText().length()<1 || !check(jtfTP.getText()) || jtfTP1.getText().length()<1 || !check(jtfTP1.getText()) ){
                              String message = "Errore nel inserimento dei campi ";
                              JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                                      JOptionPane.ERROR_MESSAGE);
                        }
                    	else {
                    		/*try {
                                parsed = format.parse(jtfP3.getText());     
                            } catch (Exception e2) {
                                // TODO Auto-generated catch block
                                e2.printStackTrace();
                            }
                            java.sql.Date data = new java.sql.Date(parsed.getTime());*/
                    		
                    		Tabella_TitoloP titpT = new Tabella_TitoloP();
                        	TitoloP titpl = new TitoloP();
                        	titpl.setId_Ciclo(Integer.parseInt(jtfTP.getText()));
                        	titpl.setId_TitoloP(Integer.parseInt(jtfTP1.getText()));
                        	titpl.setNome_Tit(jtfTP2.getText());
                        	            	
                        	titpT.persist(titpl);
                        	
                        	PreparedStatement statement = null;
                        	String query = "select * from titolo_prof";
                        	
                        		try {
                        			statement = (PreparedStatement) connection.prepareStatement(query);
                        			ResultSet rs = statement.executeQuery();
                        			tableTP.setModel(DbUtils.resultSetToTableModel(rs));
                        	        tableTP.getColumnModel().getColumn(0).setPreferredWidth(100);        	       
                        	        //tableC.getColumnModel().getColumn(6).setPreferredWidth(30);
                        	        //tableC.getColumnModel().getColumn(7).setPreferredWidth(30); 
                        		} catch ( Exception e1) {
                        			e1.printStackTrace();
                        		}
                		     jtfTP.setText("");
                		     jtfTP1.setText("");
                		     jtfTP2.setText("");
                		     
                    	  } 
                       });


/*****************************************************************************************************************************************************/	
		
       
        tabbedPane.addTab("Home", null, firstPanel, null);
        tabbedPane.addTab("Utenti", null, jpP, null);
        tabbedPane.addTab("Modalita_Form", null, jpMF, null);
        tabbedPane.addTab("Aree_Form", null, jpAF, null);
        tabbedPane.addTab("Corsi", null, jpCOF, null);
        tabbedPane.addTab("Cicli Professionali", null, jpCTP, null);
        tabbedPane.addTab("Titoli Professionali", null, jpTP, null);
        /*tabbedPane.addTab("Istruttori", null, jpI, null);
        tabbedPane.addTab("Fisioterapisti", null, jpF, null);
        tabbedPane.addTab("Reception", null, jpR, null);
        tabbedPane.addTab("Abbonamenti", null, jpA, null);
        tabbedPane.addTab("Settore", null, jpS, null);
        tabbedPane.addTab("Macchinari", null, jpM, null);
        tabbedPane.addTab("Gruppi", null, jpG, null);
        tabbedPane.addTab("Carte_Clienti", null, jpCC, null);
        tabbedPane.addTab("Schede_Allenamento", null, jpSA, null);
        tabbedPane.addTab("Visite", null, jpV, null);
        tabbedPane.addTab("Controlli_Macchinari",null, jpCM, null);
        tabbedPane.addTab("Esercitazioni Gruppo",null, jpES, null);
        tabbedPane.addTab("Metodi_Esercitazione", null, jpMES, null);
        tabbedPane.addTab("Clienti Info", null, jpClInt, null);
        tabbedPane.addTab("Istruttori Info", null, jpInt, null);
        tabbedPane.addTab("Fisioterapisti Info", null, jpFF, null);
        tabbedPane.addTab("Schede Allenameto Info", null, jpSAI, null);
        tabbedPane.addTab("Macchinari Info", null, jpMI, null);
        tabbedPane.addTab("Esercitazione Info", null, jpEI, null);
        tabbedPane.addTab("Visite Info", null, jpVI, null); */



        /*for(int i=0; i<22; i++) {
        	tabbedPane.setEnabledAt(i, false);
        }*/
        login.addActionListener(e->{
            if(nome.getText().equals("admin")&&passwordField.getText().equals("admin")){
            	 //for(int i=0;i<22;i++){
                 //    tabbedPane.setEnabledAt(i, true);
               //}
        		String message = "Benvenuti! Sei riuscito ad accedere con successo!";
        		JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
        				JOptionPane.ERROR_MESSAGE);
        		tabbedPane.setEnabledAt(0, false);
            }
        });       
        
      this.setSize(1200, 1000);
      this.setVisible(true);
    }

    
    boolean check(String s){
          try{
              Integer.parseInt(s);
              return true;
          } catch(Exception e){
              return false;
          }
    }
	
	public static void main(String[] args) {
        new TabbedPane();

	}

}

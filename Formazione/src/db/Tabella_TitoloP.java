package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.joda.time.Days;
import org.joda.time.LocalDate;

import model.AreaF;
import model.Ciclo;

import model.TitoloP;

public class Tabella_TitoloP {

	 @SuppressWarnings("unused")
     private DBConnection dataSource;
     private String tableName;
     
     public Tabella_TitoloP () {
         dataSource = new DBConnection();
         tableName="titolo_prof";  
     }
     
     public void persist(TitoloP tp) {
    	 
    	 
         Connection connection = DBConnection.getMsSQLConnection();
         PreparedStatement statement = null;
         
         Tabella_Ciclo cict = new Tabella_Ciclo();
         
         if (cict.findByPrimaryKey(tp.getId_Ciclo()) == null){
             String message = "Ciclo non esiste";
             JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                     JOptionPane.ERROR_MESSAGE);
                  new Exception("Ciclo non esiste");
         }else {
        	 
        	/* if((!findByPrimaryKey(ct.findByPrimaryKey(e.getIdCorso()) == null) &&
        			 ) */
            	 
        		 String insert = "insert into "+ tableName +" (ID_Ciclo, ID_Titolo, Titolo_Professionale) values (?,?,?)";
        		 try {
        			 statement = connection.prepareStatement(insert);
        			 statement.setInt (1, tp.getId_Ciclo());
        			 statement.setInt (2, tp.getId_TitoloP());
        			 statement.setString  (3, tp.getNome_Tit());
        			 

        			 statement.executeUpdate();
        		 }
        		 catch (SQLException s) {
        			 new Exception(s.getMessage());
        		 System.out.println("Errore : 1"+ s.getMessage());
        		 }
        		 finally {
        			 try {
        				 if (statement != null) 
        					 statement.close();
        				 if (connection!= null)
        					 connection.close();
        			 }
        			 catch (SQLException s) {
        				 new Exception(s.getMessage());
                     System.out.println("Errore"+ s.getMessage());
        			 }
        		 }
        	 }
         }
  public void update(TitoloP tp) {
         
         Connection connection = DBConnection.getMsSQLConnection();
         PreparedStatement statement;
         
         List<TitoloP> myTitles = new ArrayList();
         
         Tabella_Ciclo cict = new Tabella_Ciclo();
         Tabella_TitoloP ttp =  new Tabella_TitoloP();
         myTitles = ttp.findAll();
         
         boolean thereIsMyTitle = false;
         for(int i=0; i<myTitles.size(); i++) {
        	 if(myTitles.get(i).getId_TitoloP() == tp.getId_TitoloP()) {
        		 thereIsMyTitle = true; 
        	 }
         }
         
         
         boolean oldCiclo = ( cict.findByPrimaryKey(tp.getId_Ciclo())!= null);
         	if(oldCiclo == false && thereIsMyTitle==false ){
             String message = "Ciclo non esistente oppure titolo non esistente";
             JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
             JOptionPane.ERROR_MESSAGE);
             new Exception("Ciclo non esistente oppure titolo non esistente");
          } 
        
         String insert = "update "+ tableName +" set ID_Ciclo=?, Titolo_Professionale=? where ID_Titolo= ?";
        
         try { 
       	  statement = connection.prepareStatement(insert);
       	  statement.setInt(1, tp.getId_Ciclo());/////////BOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO
          statement.setString(2, tp.getNome_Tit());
          statement.setInt (3, tp.getId_TitoloP());
          
          statement.executeUpdate();         
         } catch (SQLException e) {
             e.printStackTrace();
           }
         
         JOptionPane.showMessageDialog(null, "Titolo aggiornato");

     }
  
  public void delete(Integer id_titP) {
      Connection connection = DBConnection.getMsSQLConnection();

      PreparedStatement statement = null;
      String insert = "delete from "+ tableName +" where  ID_Titolo= ?";
      try {
          statement = connection.prepareStatement(insert);
          statement.setInt(1,id_titP);
          statement.executeUpdate();
      }
      catch (SQLException e) {
              new Exception(e.getMessage());
          System.out.println("Errore"+ e.getMessage());
      }
      finally {
          try {
              if (statement != null) 
                  statement.close();
              if (connection!= null)
                  connection.close();
          }
          catch (SQLException e) {
              new Exception(e.getMessage());
                  System.out.println("Errore"+ e.getMessage());
          }
      }
  }
     
  public TitoloP findByPrimaryKey(Integer id_titP)  {
      TitoloP titp = null;
      
      Connection connection = DBConnection.getMsSQLConnection();
      PreparedStatement statement = null;
      String query = "select * from "+ tableName +" where ID_Titolo=? ";
      try {
          statement = connection.prepareStatement(query);
          statement.setInt(1, id_titP);
          ResultSet result = statement.executeQuery();
          if (result.next()) {
              titp = new TitoloP();
              titp.setId_Ciclo(result.getInt("ID_Ciclo")); ////////////BOOOOOOOO
              titp.setId_TitoloP(result.getInt("ID_Titolo"));
              titp.setNome_Tit(result.getString("Titolo_Professionale"));
             
         }
      }
      catch (SQLException e) {
              new Exception(e.getMessage());
          System.out.println("Errore"+ e.getMessage());
      }
      finally {
          try {
              if (statement != null) 
                  statement.close();
              if (connection!= null)
                  connection.close();
          } catch (SQLException e) {
              new Exception(e.getMessage());
                   System.out.println("Errore"+ e.getMessage());
          }
      }
      return titp;
  }  
  
  public List<TitoloP> findAll()  {
      List<TitoloP> titoli = null;
      TitoloP tit = null;
      Connection connection = DBConnection.getMsSQLConnection();
      PreparedStatement statement = null;
      String query = "select * from "+ tableName ;
      try {
          statement = connection.prepareStatement(query);
          ResultSet result = statement.executeQuery();
          if(result.next()) {
              titoli = new LinkedList<TitoloP>();
              tit = new TitoloP();
              tit.setId_Ciclo(result.getInt("ID_Ciclo")); ////////////BOOOOOOOO
              tit.setId_TitoloP(result.getInt("ID_Titolo"));
              tit.setNome_Tit(result.getString("Titolo_Professionale"));
              
          }
          
          while(result.next()) {
         	 tit = new TitoloP();
         	 tit.setId_Ciclo(result.getInt("ID_Ciclo")); ////////////BOOOOOOOO
             tit.setId_TitoloP(result.getInt("ID_Titolo"));
             tit.setNome_Tit(result.getString("Titolo_Professionale"));
         	 titoli.add(tit);
          }
      }
      catch (SQLException e) {
              new Exception(e.getMessage());
          System.out.println("Errore"+ e.getMessage());
      }
      finally {
          try {
              if (statement != null) 
                  statement.close();
              if (connection!= null)
                  connection.close();
          } catch (SQLException e) {
              new Exception(e.getMessage());
                   System.out.println("Errore"+ e.getMessage());
          }
      }
      return titoli;
  } 
  
}
     
     
     

package db;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

import model.Utente;

public class Tabella_Utente extends Utente{
	
	 @SuppressWarnings("unused")
     private DBConnection dataSource;
     private String tableName;
     
     public Tabella_Utente () {
         dataSource = new DBConnection();
         tableName="utenti";  
     }
     
     public void persist(Utente u) {
         Connection connection = DBConnection.getMsSQLConnection();

         if (findByPrimaryKey(u.getCF())!=null){
             String message = "Utente gi� inserito";
             JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                     JOptionPane.ERROR_MESSAGE);
                  new Exception("Utente gi� inserito");
         }else 
         {        
         PreparedStatement statement = null; 
         String insert = "insert into "+ tableName +" (CF, Cognome, Nome, Data_Nascita, Luogo_Nascita, Via, Civico, Citt�, CAP, Email, Cellulare) values (?,?,?,?,?,?,?,?,?,?,?)";
         try {
             statement = connection.prepareStatement(insert);
             statement.setString(1, u.getCF());
             statement.setString(2, u.getCognome());
             statement.setString(3, u.getNome());
             statement.setDate  (4, u.getDataN());
             statement.setString(5, u.getLuogoN());
             statement.setString(6, u.getVia());
			 statement.setInt   (7, u.getNrCivico());
			 statement.setString(8, u.getCitt�());
			 statement.setInt   (9, u.getCAP());
			 statement.setString(10, u.getEmail());
			 statement.setString(11, u.getCellulare());
             statement.executeUpdate();
         }
         catch (SQLException e) {
                 new Exception(e.getMessage());
             System.out.println("Errore"+ e.getMessage());
         }
         finally {
             try {
                 if (statement != null) 
                     statement.close();
                 if (connection!= null)
                     connection.close();
             }
             catch (SQLException e) {
                 new Exception(e.getMessage());
                     System.out.println("Errore"+ e.getMessage());
             }
         }
        }
     }
     
     
     public void update(Utente u) {
         
         Connection connection = DBConnection.getMsSQLConnection();
         PreparedStatement statement;
         
         Utente oldUtente = findByPrimaryKey(u.getCF());
         if (oldUtente == null){
             String message = "Utente non esistente";
             JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
             JOptionPane.ERROR_MESSAGE);
             new Exception("Utente non esiste");
          } 
        
         String insert = "update "+ tableName +" set Cognome=?, Nome=?, Data_Nascita=?, Luogo_Nascita=?, Via=?, Civico=?, Citt�=?, CAP=?, Email=?, Cellulare=? where CF= ?";
        
         try {
   
       	  statement = connection.prepareStatement(insert);
       	  statement.setString(1, u.getCognome());
          statement.setString(2, u.getNome());
          statement.setDate  (3, u.getDataN());
          statement.setString(4, u.getLuogoN());
          statement.setString(5, u.getVia());
		  statement.setInt   (6, u.getNrCivico());
		  statement.setString(7, u.getCitt�());
		  statement.setInt   (8, u.getCAP());
		  statement.setString(9, u.getEmail());
		  statement.setString(10, u.getCellulare());
          statement.setString(11, u.getCF());

             statement.executeUpdate();
             
         } catch (SQLException e) {
             e.printStackTrace();
           }
         
         JOptionPane.showMessageDialog(null, "Utente aggiornato");

     }
     
     public void delete(String CF) {
         Connection connection = DBConnection.getMsSQLConnection();

         PreparedStatement statement = null;
         String insert = "delete from "+ tableName +" where CF = ?";
         try {
             statement = connection.prepareStatement(insert);
             statement.setString(1, CF);
             statement.executeUpdate();
         }
         catch (SQLException e) {
                 new Exception(e.getMessage());
             System.out.println("Errore"+ e.getMessage());
         }
         finally {
             try {
                 if (statement != null) 
                     statement.close();
                 if (connection!= null)
                     connection.close();
             }
             catch (SQLException e) {
                 new Exception(e.getMessage());
                     System.out.println("Errore"+ e.getMessage());
             }
         }
     }
     
     public Utente findByPrimaryKey(String CF)  {
         Utente utente = null;
         
         Connection connection = DBConnection.getMsSQLConnection();
         PreparedStatement statement = null;
         String query = "select * from "+ tableName +" where CF=? ";
         try {
             statement = connection.prepareStatement(query);
             statement.setString(1, CF);
             ResultSet result = statement.executeQuery();
             if (result.next()) {
                 utente = new Utente();
                 utente.setCF(result.getString("CF"));
                 utente.setCognome(result.getString("Cognome"));
                 utente.setNome(result.getString("Nome"));
                 utente.setDataN(result.getDate("DataNascita"));
                 utente.setLuogoN(result.getString("LuogoNascita"));
                 utente.setVia(result.getString("Via"));
    			 utente.setNrCivico(result.getInt("Civico"));
    			 utente.setCitt�(result.getString("Citt�"));
    			 utente.setCAP(result.getInt("CAP"));
    			 utente.setEmail(result.getString("Email"));
    			 utente.setCellulare(result.getString("Cellullare"));
            }
         }
         catch (SQLException e) {
                 new Exception(e.getMessage());
             System.out.println("Errore"+ e.getMessage());
         }
         finally {
             try {
                 if (statement != null) 
                     statement.close();
                 if (connection!= null)
                     connection.close();
             } catch (SQLException e) {
                 new Exception(e.getMessage());
                      System.out.println("Errore"+ e.getMessage());
             }
         }
         return utente;
     }  
     
     
     public List<Utente> findAll()  {
         List<Utente> utenti = null;
         Utente utente = null;
         Connection connection = DBConnection.getMsSQLConnection();
         PreparedStatement statement = null;
         String query = "select * from "+ tableName ;
         try {
             statement = connection.prepareStatement(query);
             ResultSet result = statement.executeQuery();
             if(result.next()) {
                 utenti = new LinkedList<Utente>();
                 utente = new Utente();
                 utente.setCF(result.getString("CF"));
                 utente.setCognome(result.getString("Cognome"));
                 utente.setNome(result.getString("Nome"));
                 utente.setDataN(result.getDate("DataNascita"));
                 utente.setLuogoN(result.getString("LuogoNascita"));
                 utente.setVia(result.getString("Via"));
    			 utente.setNrCivico(result.getInt("Civico"));
    			 utente.setCitt�(result.getString("Citt�"));
    			 utente.setCAP(result.getInt("CAP"));
    			 utente.setEmail(result.getString("Email"));
    			 utente.setCellulare(result.getString("Cellullare"));
             }
             
             while(result.next()) {
            	 utente = new Utente();
            	 utente.setCF(result.getString("CF"));
            	 utente.setNome(result.getString("nome"));
            	 utente.setCognome(result.getString("cognome"));
            	 utente.setCitt�(result.getString("citt�"));
            	 utente.setVia(result.getString("via"));
            	 utente.setNrCivico(result.getInt("nrCivico"));
            	 utente.setCAP(result.getInt("CAP"));
            	 utenti.add(utente);
             }
         }
         catch (SQLException e) {
                 new Exception(e.getMessage());
             System.out.println("Errore"+ e.getMessage());
         }
         finally {
             try {
                 if (statement != null) 
                     statement.close();
                 if (connection!= null)
                     connection.close();
             } catch (SQLException e) {
                 new Exception(e.getMessage());
                      System.out.println("Errore"+ e.getMessage());
             }
         }
         return utenti;
     } 
}


package org.joda.time;

import java.util.TimeZone;

import org.joda.time.format.ISODateTimeFormat;

public class TempTest {

    private static final DateTimeZone PARIS = DateTimeZone.forID("Europe/Paris");
    private static final DateTimeZone BERLIN = DateTimeZone.forID("Europe/Berlin");
    private static final DateTimeZone SHANGHAI = DateTimeZone.forID("Asia/Shanghai");

    public static void main(String[] args) {
        
        TimeZone.setDefault(TimeZone.getTimeZone("Europe/Copenhagen"));
        
        LocalDate date1 = new LocalDate(2019, 2, 16);
        LocalDate date2 = date1.plusDays(45);
        
        System.out.println(date1);
        System.out.println(date2);
        
//        DateTime dt1 = new DateTime(2018, 10, 28, 2, 0, PARIS);
//        DateTime dt2 = dt1.withTimeAtStartOfDay();
//        System.out.println(Minutes.minutesBetween(dt1, dt2));
        
//        DateTimeZone shanghai = DateTimeZone.forID("Asia/Shanghai");
//        DateTimeZone saoPaulo = DateTimeZone.forID("Europe/Istanbul");
//        DateTime date1_sh = new DateTime(2018, 10, 21, 10, 0, 0, 0, shanghai);
//        DateTime date2_sh = new DateTime(2018, 10, 21, 11, 0, 0, 0, shanghai);
//        System.out.println("Asia/Shanghai 1: " + date1_sh.toString("yyyy-MM-dd HH:mm:ss.SSS Z"));
//        System.out.println("Asia/Shanghai 2: " + date2_sh.toString("yyyy-MM-dd HH:mm:ss.SSS Z"));
//        System.out.println("America/Sao_Paulo 1: " + date1_sh.withZone(saoPaulo).toString("yyyy-MM-dd HH:mm:ss.SSS Z"));
//        System.out.println("America/Sao_Paulo 2: " + date2_sh.withZone(saoPaulo).toString("yyyy-MM-dd HH:mm:ss.SSS Z"));
//        
//        DateTime dt1 = new DateTime(2018, 11, 3, 23, 59, 0, 0, saoPaulo);
//        DateTime dt2 = new DateTime(2018, 11, 4, 1, 1, 0, 0, saoPaulo);
//        System.out.println(dt1);
//        System.out.println(dt2);

//        System.out.println(DateTimeZone.forID("EST"));
//        System.out.println(DateTimeZone.forID("PST"));
//        
//
//        LocalDateTime base = new LocalDateTime(2012, 1, 1, 0, 0);
//        System.out.println(base);
//        System.out.println(base.toDateTime(DateTimeZone.UTC).withZone(DateTimeZone.forOffsetHours(8)).toLocalDateTime());
//        System.out.println(new DateTime(base, DateTimeZone.getDefault()));
////        2012-01-01T00:00:00.000
//        System.out.println(new DateTime(base, (DateTimeZone) null));
////        2012-01-01T00:00:00.000
//        System.out.println(new DateTime(base, DateTimeZone.forOffsetHours(8)));
////        2012-01-01T00:00:00.000
//        
//        ISODateTimeFormat.dateTimeParser().parseDateTime("2017-09-01T15:00:00Z");

//        DateTime now = DateTime.now();
//        int days = Days.daysBetween(now, now.minusDays(1)).getDays();
//        System.out.println(days);
//
//        Days value = Days.daysBetween(
//                DateTime.now(BERLIN),
//                DateTime.now(BERLIN).minusDays(1));
//        System.out.println(value.getDays());
    }

}

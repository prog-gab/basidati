package model;

import java.sql.Date;

public class EdizioneG {
	
	
	private Date dataInizio;
	private Date dataFine;
	private int nrGiornate;
	private int idCorso;
	private int idModalita;
	private String note;
	private int cod_EG; //aggiunto
	
	
	public Date getDataFine() {
		return dataFine;
	}
	
	public void setDataFine(Date dataf) {
		this.dataFine= dataf;
	}
////
	public int getCodice_EG() {
		return cod_EG ;
	}
	
	public void  setCodice_EG(int c) {
		 this.cod_EG = c ;
	}
////
	public int getNrGiornate() {
		return nrGiornate;
	}

	public void setNrGiornate(int nrGiornate) {
		this.nrGiornate = nrGiornate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public EdizioneG() {}
	
	public Date getDataInizio() {
		return dataInizio;
	}
	
	public void setDataInizio(Date dataInizio) {
		this.dataInizio = dataInizio;
	}
	
	public int getIdCorso() {
		return idCorso;
	}
	
	public void setIdCorso(int idCorso) {
		this.idCorso = idCorso;
	}

	public int getIdModalita() {
		return idModalita;
	}
	
	public void setIdModalita(int idModalita) {
		this.idModalita = idModalita;
	}
	
	
	
}

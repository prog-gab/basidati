package db;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

import model.EdizioneE;
import model.EdizioneG;
import model.Corso;

public class Tabella_EdizioneE extends EdizioneE{
	
	 @SuppressWarnings("unused")
     private DBConnection dataSource;
     private String tableName;
     
     public Tabella_EdizioneE () {
         dataSource = new DBConnection();
         tableName="ed_elearning";  
     }
     
     public void persist(EdizioneE eg) {
    	 
    	 
         Connection connection = DBConnection.getMsSQLConnection();
         PreparedStatement statement = null;
         
         Tabella_Corso ct = new Tabella_Corso();
         
         if (ct.findByPrimaryKey(eg.getCodiceCorso()) == null){
             String message = "Corso non esiste";
             JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                     JOptionPane.ERROR_MESSAGE);
                  new Exception("Corso non esiste");
         }else{
        	 
        	/* if((!findByPrimaryKey(ct.findByPrimaryKey(e.getIdCorso()) == null) &&
        			 ) */
            	 
        		 String insert = "insert into "+ tableName +" (ID_Corso, Data_Inizio, N_Crediti) values (?,?,?)";
        		 try {
        			 statement = connection.prepareStatement(insert);
        			 statement.setInt   (1, eg.getCodiceCorso());
        			 statement.setDate  (2, eg.getDataInizio());
        			 statement.setInt  (3, eg.getPunti());

        			 statement.executeUpdate();
        		 }
        		 catch (SQLException s) {
        			 new Exception(s.getMessage());
        		 System.out.println("Errore : 1"+ s.getMessage());
        		 }
        		 finally {
        			 try {
        				 if (statement != null) 
        					 statement.close();
        				 if (connection!= null)
        					 connection.close();
        			 }
        			 catch (SQLException s) {
        				 new Exception(s.getMessage());
                     System.out.println("Errore"+ s.getMessage());
        			 }
        		 }
        	 }
   
     }
     
     
     public void update(EdizioneE eg) {
         
         Connection connection = DBConnection.getMsSQLConnection();
         PreparedStatement statement;
         Tabella_EdizioneE et = new Tabella_EdizioneE();
         
         
         boolean thereIsEdizione = (et.findByPrimaryKey(eg.getCodiceCorso(), eg.getDataInizio())!= null);
         if (thereIsEdizione == false){
             String message = "Edizione non esistente.";
             JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
             JOptionPane.ERROR_MESSAGE);
             new Exception("Edizione non esiste");
          } else {
        
         String insert = "update "+ tableName +" set N_Crediti=? where ID_Corso= ? && Data_Inizio = ?";
        
         try {
   
       	  statement = connection.prepareStatement(insert);
          statement.setInt  (1 ,eg.getPunti());
       	  statement.setInt(2, eg.getCodiceCorso());
          statement.setDate(3, eg.getDataInizio());

           statement.executeUpdate();
             
         } catch (SQLException e) {
             e.printStackTrace();
           }
         
         JOptionPane.showMessageDialog(null, "Edizione E-Learnign aggiornato");
          }
     }
     
     
     public void delete(int cod_ee, Date d) {
         Connection connection = DBConnection.getMsSQLConnection();
         

         Tabella_EdizioneE ct = new Tabella_EdizioneE();
         
         if (ct.findByPrimaryKey(cod_ee, d) == null){
             String message = "Edizione E-Learning non esiste";
             JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                     JOptionPane.ERROR_MESSAGE);
                  new Exception("Edizione E-Learning non esiste");
         }else {

         PreparedStatement statement = null;
         String insert = "delete from "+ tableName +" where ID_Corso = ? and Data_Inizio = ?";
         try {
             statement = connection.prepareStatement(insert);
             statement.setInt(1, cod_ee);
             statement.setDate(2, d);
             statement.executeUpdate();
         }
         catch (SQLException e) {
                 new Exception(e.getMessage());
             System.out.println("Errore"+ e.getMessage());
         }
         finally {
             try {
                 if (statement != null) 
                     statement.close();
                 if (connection!= null)
                     connection.close();
             }
             catch (SQLException e) {
                 new Exception(e.getMessage());
                     System.out.println("Errore"+ e.getMessage());
             }
         }
         }
     }
     
     
     public EdizioneE findByPrimaryKey(int codiceCorso, Date data)  {
    	 EdizioneE ed = null;
         
         Connection connection = DBConnection.getMsSQLConnection();
         PreparedStatement statement = null;
         String query = "select * from "+ tableName +" where ID_Corso=? and Data_Inizio=?";
         try {
             statement = connection.prepareStatement(query);
             statement.setInt(1, codiceCorso);
             statement.setDate(2, data);
             ResultSet result = statement.executeQuery();
             if (result.next()) {
                 ed = new EdizioneE();
                 ed.setCodiceCorso(result.getInt("ID_Corso"));
                 ed.setDataInizio(result.getDate("Data_Inizio"));
                 ed.setPunti(result.getInt("N_Crediti"));
                 ed.setCodiceEE(result.getInt("Cod_EE"));

             }
         }catch (SQLException e) {
                 new Exception(e.getMessage());
             System.out.println("Errore"+ e.getMessage());
         }
         finally {
             try {
                 if (statement != null) 
                     statement.close();
                 if (connection!= null)
                     connection.close();
             } catch (SQLException e) {
                 new Exception(e.getMessage());
                      System.out.println("Errore"+ e.getMessage());
             }
         
         }
         
         return ed;
     }  
    
     
     /*
     public List<Utente> findAll()  {
         List<Utente> utenti = null;
         Utente utente = null;
         Connection connection = DBConnection.getMsSQLConnection();
         PreparedStatement statement = null;
         String query = "select * from "+ tableName ;
         try {
             statement = connection.prepareStatement(query);
             ResultSet result = statement.executeQuery();
             if(result.next()) {
                 utenti = new LinkedList<Utente>();
                 utente = new Utente();
                 utente.setCF(result.getString("CF"));
                 utente.setCognome(result.getString("Cognome"));
                 utente.setNome(result.getString("Nome"));
                 utente.setDataN(result.getDate("DataNascita"));
                 utente.setLuogoN(result.getString("LuogoNascita"));
                 utente.setVia(result.getString("Via"));
    			 utente.setNrCivico(result.getInt("Civico"));
    			 utente.setCitt�(result.getString("Citt�"));
    			 utente.setCAP(result.getInt("CAP"));
    			 utente.setEmail(result.getString("Email"));
    			 utente.setCellulare(result.getString("Cellullare"));
             }
             
             while(result.next()) {
            	 utente = new Utente();
            	 utente.setCF(result.getString("CF"));
            	 utente.setNome(result.getString("nome"));
            	 utente.setCognome(result.getString("cognome"));
            	 utente.setCitt�(result.getString("citt�"));
            	 utente.setVia(result.getString("via"));
            	 utente.setNrCivico(result.getInt("nrCivico"));
            	 utente.setCAP(result.getInt("CAP"));
            	 utenti.add(utente);
             }
         }
         catch (SQLException e) {
                 new Exception(e.getMessage());
             System.out.println("Errore"+ e.getMessage());
         }
         finally {
             try {
                 if (statement != null) 
                     statement.close();
                 if (connection!= null)
                     connection.close();
             } catch (SQLException e) {
                 new Exception(e.getMessage());
                      System.out.println("Errore"+ e.getMessage());
             }
         }
         return utenti;
     } 
     */

     
     
}


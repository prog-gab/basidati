package model;

public class ModalitaF {
	private Integer id_modalita;
	private String Tipologia;
	
	public ModalitaF() {}

	public Integer getId_modalita() {
		return id_modalita;
	}

	public void setId_modalita(Integer id_modalita) {
		this.id_modalita = id_modalita;
	}

	public String getTipologia() {
		return Tipologia;
	}

	public void setTipologia(String tipologia) {
		Tipologia = tipologia;
	}
	
	
}

package model;

public class TitoloP {
	private int id_Ciclo;
	private int id_TitoloP;
	private String  nome_Tit;
	
	public TitoloP() {}

	public int getId_TitoloP() {
		return id_TitoloP;
	}

	public void setId_TitoloP(int id_TitoloP) {
		this.id_TitoloP = id_TitoloP;
	}

	public String getNome_Tit() {
		return nome_Tit;
	}

	public void setNome_Tit(String nome_Tit) {
		this.nome_Tit = nome_Tit;
	}

	public int getId_Ciclo() {
		return id_Ciclo;
	}

	public void setId_Ciclo(int id_Ciclo) {
		this.id_Ciclo = id_Ciclo;
	}

	
	

}

package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import model.ModalitaF;



public class Tabella_ModalitaF {
	@SuppressWarnings("unused")
    private DBConnection dataSource;
    private String tableName;
    
    public Tabella_ModalitaF () {
        dataSource = new DBConnection();
        tableName="modalita_form";  
    }
    
    public void persist(ModalitaF mf) {
        Connection connection = DBConnection.getMsSQLConnection();

        ModalitaF oldModalita = findByPrimaryKey(mf.getId_modalita());
        if (oldModalita != null) {
        	String message = "Modalita formativa esistente";////////////////////////////////////
            JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
            JOptionPane.ERROR_MESSAGE);
            new Exception("La Modalita non esiste");
        }else {
        	if(oldModalita == null && mf.getTipologia().length()<1){
        		String message = "Campo vuoto";
                JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                JOptionPane.ERROR_MESSAGE);
                new Exception("Campo vuoto");
        	}else {
                
        		PreparedStatement statement = null; 
        		String insert = "insert into "+ tableName +" (ID_Modalita, Tipologia) values (?,?)";
        		try {
        			statement = connection.prepareStatement(insert);
        			statement.setInt(1, mf.getId_modalita());
        			statement.setString(2, mf.getTipologia());
            
        			statement.executeUpdate();
        		}
        		catch (SQLException e) {
        			new Exception(e.getMessage());
        			System.out.println("Errore"+ e.getMessage());
        		}
        		finally {
        			try {
        				if (statement != null) 
        					statement.close();
        				if (connection!= null)
        					connection.close();
        			}
        			catch (SQLException e) {
        				new Exception(e.getMessage());
        				System.out.println("Errore"+ e.getMessage());
        			}
        	 	}
        	}
       	}
    }
    
    public void update(ModalitaF mf) {
        
        Connection connection = DBConnection.getMsSQLConnection();
        PreparedStatement statement;
        
        ModalitaF oldModalita = findByPrimaryKey(mf.getId_modalita());
        if (oldModalita == null) {
        	String message = "Modalita formativa non esistente";
            JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
            JOptionPane.ERROR_MESSAGE);
            new Exception("La Modalita non esiste");
        }else {
        	if(oldModalita != null && mf.getTipologia().length()<1){
        		String message = "Campo vuoto";
                JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                JOptionPane.ERROR_MESSAGE);
                new Exception("Campo vuoto");
        	}else {

               String insert = "update "+ tableName +" set Tipologia=?  where ID_Modalita= ?";

               	try {
  
               		statement = connection.prepareStatement(insert);
               		statement.setString(1, mf.getTipologia());
               		statement.setInt(2, mf.getId_modalita());

               		statement.executeUpdate();
            
               	} catch (SQLException e) {
               		e.printStackTrace();
               	}
        
               	JOptionPane.showMessageDialog(null, "Modalita formativa aggiornata");
         }
        }
    }
    
    public void delete(Integer id_modalita) {
        Connection connection = DBConnection.getMsSQLConnection();

        PreparedStatement statement = null;
        String insert = "delete from "+ tableName +" where  ID_Modalita= ?";
        try {
            statement = connection.prepareStatement(insert);
            statement.setInt(1,id_modalita);
            statement.executeUpdate();
        }
        catch (SQLException e) {
                new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                    System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    
    
    public ModalitaF findByPrimaryKey(Integer id_modalita)  {
        ModalitaF modf = null;
        
        Connection connection = DBConnection.getMsSQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where ID_Modalita=? ";
        try {
            statement = connection.prepareStatement(query);
            statement.setInt(1, id_modalita);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                modf = new ModalitaF();
                modf.setId_modalita(result.getInt("ID_Modalita"));
                modf.setTipologia(result.getString("Tipologia"));
               
           }
        }
        catch (SQLException e) {
                new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                     System.out.println("Errore"+ e.getMessage());
            }
        }
        return modf;
    }  
    
    public List<ModalitaF> findAll()  {
        List<ModalitaF> modalita = null;
        ModalitaF mod = null;
        Connection connection = DBConnection.getMsSQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName ;
        try {
            statement = connection.prepareStatement(query);
            ResultSet result = statement.executeQuery();
            if(result.next()) {
                modalita = new LinkedList<ModalitaF>();
                mod = new ModalitaF();
                	mod.setId_modalita(result.getInt("ID_Modalita"));
                	mod.setTipologia(result.getString("Tipologia"));
                
            }
            
            while(result.next()) {
           	 	mod = new ModalitaF();
           	 		mod.setId_modalita(result.getInt("ID_Modalita"));
           	 		mod.setTipologia(result.getString("Tipologia"));;
           	 modalita.add(mod);
            }
        }
        catch (SQLException e) {
                new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                     System.out.println("Errore"+ e.getMessage());
            }
        }
        return modalita;
    } 
}

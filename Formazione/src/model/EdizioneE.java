package model;

import java.sql.Date;

public class EdizioneE {
	
	private int codiceCorso;
	private Date dataInizio;
	private int punti;
	private int codiceEE;
	
	public int getCodiceCorso() {
		return codiceCorso;
	}
	
	public int getCodiceEE() {
		return codiceEE;
	}

	public void setCodiceEE(int codiceEE) {
		this.codiceEE = codiceEE;
	}

	public void setCodiceCorso(int codiceCorso) {
		this.codiceCorso = codiceCorso;
	}
	
	public Date getDataInizio() {
		return dataInizio;
	}
	
	public void setDataInizio(Date dataInizio) {
		this.dataInizio = dataInizio;
	}
	
	public int getPunti() {
		return punti;
	}
	
	public void setPunti(int punti) {
		this.punti = punti;
	}
	
	

}

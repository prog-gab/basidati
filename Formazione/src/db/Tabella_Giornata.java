package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import model.EdizioneG;
import model.Giornata;
import model.ModalitaF;



public class Tabella_Giornata {
	@SuppressWarnings("unused")
    private DBConnection dataSource;
    private String tableName;
    
    public Tabella_Giornata () {
        dataSource = new DBConnection();
        tableName="giornata";  
    }
    
    public void persist(Giornata g) {
        Connection connection = DBConnection.getMsSQLConnection();
        
        Tabella_EdizioneG te = new Tabella_EdizioneG();
        
        if (te.findByPrimaryKey(g.getCodiceEdizione()) == null) {
        	String message = "Edizione non esistente";
            JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
            JOptionPane.ERROR_MESSAGE);
            new Exception("La Modalita non esiste");
        }else {
                
        		PreparedStatement statement = null; 
        		String insert = "insert into "+ tableName +" (Cod_Edizione, Data_Giornata, Argomento, Durata_Ore) values (?,?,?,?)";
        		try {
        			statement = connection.prepareStatement(insert);
        			statement.setInt(1, g.getCodiceEdizione());
        			statement.setDate(2, g.getDataGiornata());
        			statement.setString(3, g.getArgomento());
        			statement.setInt(4, g.getDurataOre());

        			statement.executeUpdate();
        		}
        		catch (SQLException e) {
        			new Exception(e.getMessage());
        			System.out.println("Errore ? "+ e.getMessage());
        		}
        		finally {
        			try {
        				if (statement != null) 
        					statement.close();
        				if (connection!= null)
        					connection.close();
        			}
        			catch (SQLException e) {
        				new Exception(e.getMessage());
        				System.out.println("Errore - "+ e.getMessage());
        			}
        	 	}
        	}
    }
    
    /*
    public void update(Giornata g) {
        
        Connection connection = DBConnection.getMsSQLConnection();
        PreparedStatement statement;
        
        ModalitaF oldModalita = findByPrimaryKey(mf.getId_modalita());
        
        if (oldModalita == null) {
        	String message = "Modalita formativa non esistente";
            JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
            JOptionPane.ERROR_MESSAGE);
            new Exception("La Modalita non esiste");
        }else {
        	if(oldModalita != null && mf.getTipologia().length()<1){
        		String message = "Campo vuoto";
                JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                JOptionPane.ERROR_MESSAGE);
                new Exception("Campo vuoto");
        	}else {

               String insert = "update "+ tableName +" set Tipologia=?  where ID_Modalita= ?";

               	try {
  
               		statement = connection.prepareStatement(insert);
               		statement.setString(1, mf.getTipologia());
               		statement.setInt(2, mf.getId_modalita());

               		statement.executeUpdate();
            
               	} catch (SQLException e) {
               		e.printStackTrace();
               	}
        
               	JOptionPane.showMessageDialog(null, "Modalita formativa aggiornata");
         }
        }
    }
    */
    
    
    public void delete(Integer id_modalita) {
        Connection connection = DBConnection.getMsSQLConnection();

        PreparedStatement statement = null;
        
        
        Tabella_Giornata tg = new Tabella_Giornata();
        
        if(tg.findByCodiceGiornata(id_modalita) == null) {
        	String message = "Giornata non esistente";
            JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
            JOptionPane.ERROR_MESSAGE);
            new Exception("Giornata non esiste");        	
        }else {          
        	String insert = "delete from "+ tableName +" where  Cod_Giornata= ?";
        	try {
        		statement = connection.prepareStatement(insert);
        		statement.setInt(1,id_modalita);
        		statement.executeUpdate();
        	}
        	catch (SQLException e) {
                new Exception(e.getMessage());
                System.out.println("Errore"+ e.getMessage());
        	}
        	finally {
        		try {
        			if (statement != null) 
        				statement.close();
        			if (connection!= null)
        				connection.close();
        		}
        		catch (SQLException e) {
        			new Exception(e.getMessage());
                    	System.out.println("Errore"+ e.getMessage());
        		}	
        	}
        }	
    }
    
    
    public Giornata findByCodiceGiornata(Integer codice)  {
    	Giornata g = null;
        
        Connection connection = DBConnection.getMsSQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +"  where  Cod_Giornata= ?";
        try {
            statement = connection.prepareStatement(query);
            statement.setInt(1, codice);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                g = new Giornata();
                g.setCodiceGiornata(result.getInt("Cod_Giornata"));
                g.setCodiceEdizione(result.getInt("Cod_Edizione"));
                g.setDataGiornata(result.getDate("Data_Giornata"));
                g.setArgomento(result.getString("Argomento"));
                g.setDurataOre(result.getInt("Durata_Ore"));             
           }
        }
        catch (SQLException e) {
                new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                     System.out.println("Errore"+ e.getMessage());
            }
        }
        return g;
    }  
    
    
    
    
    /*
    public List<ModalitaF> findAll()  {
        List<ModalitaF> modalita = null;
        ModalitaF mod = null;
        Connection connection = DBConnection.getMsSQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName ;
        try {
            statement = connection.prepareStatement(query);
            ResultSet result = statement.executeQuery();
            if(result.next()) {
                modalita = new LinkedList<ModalitaF>();
                mod = new ModalitaF();
                	mod.setId_modalita(result.getInt("ID_Modalita"));
                	mod.setTipologia(result.getString("Tipologia"));
                
            }
            
            while(result.next()) {
           	 	mod = new ModalitaF();
           	 		mod.setId_modalita(result.getInt("ID_Modalita"));
           	 		mod.setTipologia(result.getString("Tipologia"));;
           	 modalita.add(mod);
            }
        }
        catch (SQLException e) {
                new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                     System.out.println("Errore"+ e.getMessage());
            }
        }
        return modalita;
    } 
    
    */
}

package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import model.Ciclo;

public class Tabella_Ciclo {
	
	@SuppressWarnings("unused")
    private DBConnection dataSource;
    private String tableName;
    
    public Tabella_Ciclo () {
        dataSource = new DBConnection();
        tableName="ciclo";  
    }
    
    public void persist(Ciclo cicP) {
        Connection connection = DBConnection.getMsSQLConnection();

        Ciclo oldCiclo = findByPrimaryKey(cicP.getId_Ciclo());
        if (oldCiclo != null) {
        	String message = "Ciclo Titoli Professionali esistente";////////////////////////////////////
            JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
            JOptionPane.ERROR_MESSAGE);
            new Exception("Il Ciclo Titoli Professionali non esiste");
        }else {
        	if(oldCiclo == null && cicP.getDen_Ciclo().length()<1){
        		String message = "Campo vuoto";
                JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                JOptionPane.ERROR_MESSAGE);
                new Exception("Campo vuoto");
        	}else {
                
        		PreparedStatement statement = null; 
        		String insert = "insert into "+ tableName +" (ID_Ciclo, Den_Ciclo, Descrizione) values (?,?,?)";
        		try {
        			statement = connection.prepareStatement(insert);
        			statement.setInt(1, cicP.getId_Ciclo());
        			statement.setString(2, cicP.getDen_Ciclo());
        			statement.setString(3, cicP.getDescr());
            
        			statement.executeUpdate();
        		}
        		catch (SQLException e) {
        			new Exception(e.getMessage());
        			System.out.println("Errore"+ e.getMessage());
        		}
        		finally {
        			try {
        				if (statement != null) 
        					statement.close();
        				if (connection!= null)
        					connection.close();
        			}
        			catch (SQLException e) {
        				new Exception(e.getMessage());
        				System.out.println("Errore"+ e.getMessage());
        			}
        	 	}
        	}
       	}
    }

    
    public void update(Ciclo ctp) {
        
        Connection connection = DBConnection.getMsSQLConnection();
        PreparedStatement statement;
        
        Ciclo oldCiclo = findByPrimaryKey(ctp.getId_Ciclo());
        if (oldCiclo == null) {
        	String message = "Ciclo Titoli Professionali non esistente";
            JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
            JOptionPane.ERROR_MESSAGE);
            new Exception("Il Ciclo Titoli Professionali  non esiste");
        }else {
        	if(oldCiclo != null && ctp.getDen_Ciclo().length()<1){
        		String message = "Campo vuoto";
                JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                JOptionPane.ERROR_MESSAGE);
                new Exception("Campo vuoto");
        	}else {
        		
               String insert = "update "+ tableName +" set Den_Ciclo=? ,Descrizione=?  where ID_Ciclo=?";

               	try {
  
               		statement = connection.prepareStatement(insert);
               		statement.setString(1, ctp.getDen_Ciclo());
               		statement.setString(2, ctp.getDescr());
               		statement.setInt(3, ctp.getId_Ciclo());

               		statement.executeUpdate();
            
               	} catch (SQLException e) {
               		e.printStackTrace();
               	}
        
               	JOptionPane.showMessageDialog(null, "Ciclo Titoli Professionali aggiornato");
         }
        }
    }
    
    public void delete(Integer id_ciclo) {
        Connection connection = DBConnection.getMsSQLConnection();

        PreparedStatement statement = null;
        String insert = "delete from "+ tableName +" where  ID_Ciclo= ?";
        try {
            statement = connection.prepareStatement(insert);
            statement.setInt(1,id_ciclo);
            statement.executeUpdate();
        }
        catch (SQLException e) {
                new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                    System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public Ciclo findByPrimaryKey(Integer id_ciclo)  {
        Ciclo cicp = null;
        
        Connection connection = DBConnection.getMsSQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where ID_Ciclo=? ";
        try {
            statement = connection.prepareStatement(query);
            statement.setInt(1, id_ciclo);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
            	cicp = new Ciclo();
            	cicp.setId_Ciclo(result.getInt("ID_Ciclo"));
            	cicp.setDen_Ciclo(result.getString("Den_Ciclo"));
            	cicp.setDescr(result.getString("Descrizione"));
               
           }
        }
        catch (SQLException e) {
                new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                     System.out.println("Errore"+ e.getMessage());
            }
        }
        return cicp;
    }  
    
    public List<Ciclo> findAll()  {
        List<Ciclo> cicli = null;
        Ciclo ciclo = null;
        Connection connection = DBConnection.getMsSQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName ;
        try {
            statement = connection.prepareStatement(query);
            ResultSet result = statement.executeQuery();
            if(result.next()) {
                cicli = new LinkedList<Ciclo>();
                ciclo= new Ciclo();
                	ciclo.setId_Ciclo(result.getInt("ID_Ciclo"));
                	ciclo.setDen_Ciclo(result.getString("Den_Ciclo"));
                	ciclo.setDescr(result.getString("Descrizione"));
                
            }
            
            while(result.next()) {
            	ciclo = new Ciclo();
            		ciclo.setId_Ciclo(result.getInt("ID_Ciclo"));
            		ciclo.setDen_Ciclo(result.getString("Den_Ciclo"));
                	ciclo.setDescr(result.getString("Descrizione"));
           	 cicli.add(ciclo);
            }
        }
        catch (SQLException e) {
                new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                     System.out.println("Errore"+ e.getMessage());
            }
        }
        return cicli;
    } 
    
}

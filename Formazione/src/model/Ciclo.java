package model;

public class Ciclo {
	private int id_Ciclo;
	private String den_Ciclo;	
	private String descr;
	
	
	public Ciclo() {}


	public int getId_Ciclo() {
		return id_Ciclo;
	}


	public void setId_Ciclo(int id_Ciclo) {
		this.id_Ciclo = id_Ciclo;
	}


	public String getDen_Ciclo() {
		return den_Ciclo;
	}


	public void setDen_Ciclo(String den_Ciclo) {
		this.den_Ciclo = den_Ciclo;
	}


	public String getDescr() {
		return descr;
	}


	public void setDescr(String descr) {
		this.descr = descr;
	}

}

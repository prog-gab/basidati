package db;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import db.DBConnection;
import model.AreaF;

public class Tabella_AreaF {
	@SuppressWarnings("unused")
    private DBConnection dataSource;
    private String tableName;
    
    public Tabella_AreaF () {
        dataSource = new DBConnection();
        tableName="area_form"; 
	}
    
    
    public void persist(AreaF af) {
        Connection connection = DBConnection.getMsSQLConnection();

        AreaF oldArea = findByPrimaryKey(af.getId_area());
        if (oldArea != null) {
        	String message = "Modalita formativa esistente";
            JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
            JOptionPane.ERROR_MESSAGE);
            new Exception("La Modalita non esiste");
        }else {
        	if(oldArea == null && af.getDen_area().length()<1){
        		String message = "Campo vuoto";
                JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                JOptionPane.ERROR_MESSAGE);
                new Exception("Campo vuoto");
        	}else {       
        		PreparedStatement statement = null; 
        		String insert = "insert into "+ tableName +" (ID_Area, Den_Area) values (?,?)";
        		try {
        			statement = connection.prepareStatement(insert);
        			statement.setInt(1, af.getId_area());
        			statement.setString(2, af.getDen_area());
            
        			statement.executeUpdate();
        		}
        		catch (SQLException e) {
        			new Exception(e.getMessage());
        			System.out.println("Errore"+ e.getMessage());
        		}
        		finally {
        			try {
        				if (statement != null) 
        					statement.close();
        				if (connection!= null)
        					connection.close();
        			}	
        			catch (SQLException e) {
        				new Exception(e.getMessage());
        				System.out.println("Errore"+ e.getMessage());
        			}
        		}
        	}
        }
    }
    
    
public void update(AreaF af) {
        
        Connection connection = DBConnection.getMsSQLConnection();
        PreparedStatement statement;
        
        AreaF oldArea = findByPrimaryKey(af.getId_area());
        if (oldArea == null) {
        	String message = "Area formativa non esistente";
            JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
            JOptionPane.ERROR_MESSAGE);
            new Exception("L'Area non esiste");
        }else {
        	if(oldArea != null && af.getDen_area().length()<1){
        		String message = "Campo vuoto";
                JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                JOptionPane.ERROR_MESSAGE);
                new Exception("Campo vuoto");
        	}else  { 
       
               String insert = "update "+ tableName +" set Den_Area=?  where ID_Area= ?";

        try {
  
      	  statement = connection.prepareStatement(insert);
      	  statement.setString(1, af.getDen_area());
          statement.setInt(2, af.getId_area());

          statement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
          }
        
        JOptionPane.showMessageDialog(null, "Area formativa aggiornata");
        }
       }
    }
    
    public void delete(Integer id_area) {
        Connection connection = DBConnection.getMsSQLConnection();

        PreparedStatement statement = null;
        String insert = "delete from "+ tableName +" where  ID_Area= ?";
        try {
            statement = connection.prepareStatement(insert);
            statement.setInt(1,id_area);
            statement.executeUpdate();
        }
        catch (SQLException e) {
                new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            }
            catch (SQLException e) {
                new Exception(e.getMessage());
                    System.out.println("Errore"+ e.getMessage());
            }
        }
    }
    
    public AreaF findByPrimaryKey(Integer id_area)  {
        AreaF areaf = null;
        
        Connection connection = DBConnection.getMsSQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName +" where ID_Area=? ";
        try {
            statement = connection.prepareStatement(query);
            statement.setInt(1, id_area);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                areaf = new AreaF();
                areaf.setId_area(result.getInt("ID_Areaa"));
                areaf.setDen_area(result.getString("Den_Area"));
               
           }
        }
        catch (SQLException e) {
                new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                     System.out.println("Errore"+ e.getMessage());
            }
        }
        return areaf;
    }  
    
    public List<AreaF> findAll()  {
        List<AreaF> aree = null;
        AreaF area = null;
        Connection connection = DBConnection.getMsSQLConnection();
        PreparedStatement statement = null;
        String query = "select * from "+ tableName ;
        try {
            statement = connection.prepareStatement(query);
            ResultSet result = statement.executeQuery();
            if(result.next()) {
                aree = new LinkedList<AreaF>();
                area = new AreaF();
                area.setId_area(result.getInt("ID_Area"));
                area.setDen_area(result.getString("Den_Area"));
                
            }
            
            while(result.next()) {
           	 area = new AreaF();
           	 area.setId_area(result.getInt("ID_Modalita"));
             area.setDen_area(result.getString("Tipologia"));
           	 aree.add(area);
            }
        }
        catch (SQLException e) {
                new Exception(e.getMessage());
            System.out.println("Errore"+ e.getMessage());
        }
        finally {
            try {
                if (statement != null) 
                    statement.close();
                if (connection!= null)
                    connection.close();
            } catch (SQLException e) {
                new Exception(e.getMessage());
                     System.out.println("Errore"+ e.getMessage());
            }
        }
        return aree;
    } 
    
}

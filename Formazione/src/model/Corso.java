package model;

public class Corso {
	private Integer id_corso;
	private String titolo;
	private String present;
	
	public Corso() {}
	
	public Integer getId_corso() {
		return id_corso;
	}
	public void setId_corso(Integer id_corso) {
		this.id_corso = id_corso;
	}
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public String getPresent() {
		return present;
	}
	public void setPresent(String present) {
		this.present = present;
	}
}

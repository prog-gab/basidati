package model;

public class AreaF {
	private Integer id_area;
	private String den_area;
	
	public AreaF() {}

	public Integer getId_area() {
		return id_area;
	}

	public void setId_area(Integer id_area) {
		this.id_area = id_area;
	}

	public String getDen_area() {
		return den_area;
	}

	public void setDen_area(String den_area) {
		this.den_area = den_area;
	}

}

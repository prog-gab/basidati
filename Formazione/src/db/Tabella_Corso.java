package db;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import model.Corso;

public class Tabella_Corso {
		@SuppressWarnings("unused")
	    private DBConnection dataSource;
	    private String tableName;
	    
	    public Tabella_Corso () {
	        dataSource = new DBConnection();
	        tableName="corso";  
	    }
	    
	    public void persist(Corso c) {
	        Connection connection = DBConnection.getMsSQLConnection();

	        Corso oldCorso = findByPrimaryKey(c.getId_corso());
	        if (oldCorso != null) {
	        	String message = "Corso formativo esistente";////////////////////////////////////
	            JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
	            JOptionPane.ERROR_MESSAGE);
	            new Exception("Il Corso non esiste");
	        }else {
	        	if(oldCorso == null && c.getTitolo().length()<1){
	        		String message = "Campo vuoto";
	                JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
	                JOptionPane.ERROR_MESSAGE);
	                new Exception("Campo vuoto");
	        	}else {
	                
	        		PreparedStatement statement = null; 
	        		String insert = "insert into "+ tableName +" (ID_Corso, Titolo,Presentazione) values (?,?,?)";
	        		try {
	        			statement = connection.prepareStatement(insert);
	        			statement.setInt(1, c.getId_corso());
	        			statement.setString(2, c.getTitolo());
	        			statement.setString(3, c.getPresent());
	            
	        			statement.executeUpdate();
	        		}
	        		catch (SQLException e) {
	        			new Exception(e.getMessage());
	        			System.out.println("Errore"+ e.getMessage());
	        		}
	        		finally {
	        			try {
	        				if (statement != null) 
	        					statement.close();
	        				if (connection!= null)
	        					connection.close();
	        			}
	        			catch (SQLException e) {
	        				new Exception(e.getMessage());
	        				System.out.println("Errore"+ e.getMessage());
	        			}
	        	 	}
	        	}
	       	}
	    }
	    
	    public void update(Corso cof) {
	        
	        Connection connection = DBConnection.getMsSQLConnection();
	        PreparedStatement statement;
	        
	        Corso oldCorso = findByPrimaryKey(cof.getId_corso());
	        if (oldCorso == null) {
	        	String message = "Corso formativo non esistente";
	            JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
	            JOptionPane.ERROR_MESSAGE);
	            new Exception("Il Corso non esiste");
	        }else {
	        	if(oldCorso != null && cof.getTitolo().length()<1){
	        		String message = "Campo vuoto";
	                JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
	                JOptionPane.ERROR_MESSAGE);
	                new Exception("Campo vuoto");
	        	}else {

	               String insert = "update "+ tableName +" set Titolo=?  where ID_Corso= ?";

	               	try {
	  
	               		statement = connection.prepareStatement(insert);
	               		statement.setString(1, cof.getTitolo());
	               		statement.setInt(2, cof.getId_corso());

	               		statement.executeUpdate();
	            
	               	} catch (SQLException e) {
	               		e.printStackTrace();
	               	}
	        
	               	JOptionPane.showMessageDialog(null, "Corso formativo aggiornata");
	         }
	        }
	    }
	    
	    public void delete(Integer id_corso) {
	        Connection connection = DBConnection.getMsSQLConnection();

	        PreparedStatement statement = null;
	        String insert = "delete from "+ tableName +" where  ID_Corso= ?";
	        try {
	            statement = connection.prepareStatement(insert);
	            statement.setInt(1,id_corso);
	            statement.executeUpdate();
	        }
	        catch (SQLException e) {
	                new Exception(e.getMessage());
	            System.out.println("Errore"+ e.getMessage());
	        }
	        finally {
	            try {
	                if (statement != null) 
	                    statement.close();
	                if (connection!= null)
	                    connection.close();
	            }
	            catch (SQLException e) {
	                new Exception(e.getMessage());
	                    System.out.println("Errore"+ e.getMessage());
	            }
	        }
	    }
	    public Corso findByPrimaryKey(Integer id_corso)  {
	        Corso cof = null;
	        
	        Connection connection = DBConnection.getMsSQLConnection();
	        PreparedStatement statement = null;
	        String query = "select * from "+ tableName +" where ID_Corso=? ";
	        try {
	            statement = connection.prepareStatement(query);
	            statement.setInt(1, id_corso);
	            ResultSet result = statement.executeQuery();
	            if (result.next()) {
	                cof = new Corso();
	                cof.setId_corso(result.getInt("ID_Corso"));
	                cof.setTitolo(result.getString("Titolo"));
	                cof.setPresent(result.getString("Presentazione"));
	               
	           }
	        }
	        catch (SQLException e) {
	                new Exception(e.getMessage());
	            System.out.println("Errore"+ e.getMessage());
	        }
	        finally {
	            try {
	                if (statement != null) 
	                    statement.close();
	                if (connection!= null)
	                    connection.close();
	            } catch (SQLException e) {
	                new Exception(e.getMessage());
	                     System.out.println("Errore"+ e.getMessage());
	            }
	        }
	        return cof;
	    }  
	    
	    public List<Corso> findAll()  {
	        List<Corso> corso = null;
	        Corso cof = null;
	        Connection connection = DBConnection.getMsSQLConnection();
	        PreparedStatement statement = null;
	        String query = "select * from "+ tableName ;
	        try {
	            statement = connection.prepareStatement(query);
	            ResultSet result = statement.executeQuery();
	            if(result.next()) {
	                corso = new LinkedList<Corso>();
	                cof = new Corso();
	                	cof.setId_corso(result.getInt("ID_Corso"));
	                	cof.setTitolo(result.getString("Titolo"));
	                	cof.setPresent(result.getString("Presentazione"));
	                
	            }
	            
	            while(result.next()) {
	            	 cof = new Corso();
		                cof.setId_corso(result.getInt("ID_Corso"));
		                cof.setTitolo(result.getString("Titolo"));
		                cof.setPresent(result.getString("Presentazione"));
	           	 corso.add(cof);
	            }
	        }
	        catch (SQLException e) {
	                new Exception(e.getMessage());
	            System.out.println("Errore"+ e.getMessage());
	        }
	        finally {
	            try {
	                if (statement != null) 
	                    statement.close();
	                if (connection!= null)
	                    connection.close();
	            } catch (SQLException e) {
	                new Exception(e.getMessage());
	                     System.out.println("Errore"+ e.getMessage());
	            }
	        }
	        return corso;
	    } 
	    
	    
}

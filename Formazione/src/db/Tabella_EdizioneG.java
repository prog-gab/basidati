package db;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;

import org.joda.time.Days;
import org.joda.time.LocalDate;

import model.EdizioneG;


public class Tabella_EdizioneG extends EdizioneG{
	
	 @SuppressWarnings("unused")
     private DBConnection dataSource;
     private String tableName;
     
     public Tabella_EdizioneG () {
         dataSource = new DBConnection();
         tableName="ed_giornata";  
     }
     
     public void persist(EdizioneG eg) {
    	 
         Connection connection = DBConnection.getMsSQLConnection();
         PreparedStatement statement = null;
         
         Tabella_Corso ct = new Tabella_Corso();
         
         if (ct.findByPrimaryKey(eg.getIdCorso()) == null){
             String message = "Corso non esiste";
             JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                     JOptionPane.ERROR_MESSAGE);
                  new Exception("Corso non esiste");
         }else{
        	 if( Days.daysBetween(new LocalDate(eg.getDataInizio()), new LocalDate(eg.getDataFine())).getDays() <= 0 ||
        			 Days.daysBetween(new LocalDate(eg.getDataInizio()), new LocalDate(eg.getDataFine())).getDays() < eg.getNrGiornate()) {
        		 
        		 System.out.print("Days: "+ eg.getDataInizio().toString());
        		 	String message = "Inserimento intervallo edizione non corretto";
        		 	JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
                         JOptionPane.ERROR_MESSAGE);
                      new Exception("Inserimento intervallo edizione non corretto");
        	 }else {
        	 
        	/* if((!findByPrimaryKey(ct.findByPrimaryKey(e.getIdCorso()) == null) &&
        			 ) */
            	 
        		 String insert = "insert into "+ tableName +" (ID_Corso, Data_I, Data_F, N_Giornate, Note_E) values (?,?,?,?,?)";
        		 try {
        			 statement = connection.prepareStatement(insert);
        			 statement.setInt   (1, eg.getIdCorso());
        			 statement.setDate  (2, eg.getDataInizio());
        			 statement.setDate  (3, eg.getDataFine());
        			 statement.setInt   (4, eg.getNrGiornate());
        			 statement.setString(5, eg.getNote());

        			 statement.executeUpdate();
        		 }
        		 catch (SQLException s) {
        			 new Exception(s.getMessage());
        		 System.out.println("Errore : 1"+ s.getMessage());
        		 }
        		 finally {
        			 try {
        				 if (statement != null) 
        					 statement.close();
        				 if (connection!= null)
        					 connection.close();
        			 }
        			 catch (SQLException s) {
        				 new Exception(s.getMessage());
                     System.out.println("Errore"+ s.getMessage());
        			 }
        		 }
        	 }
         }
     }
     
    
     public void update(EdizioneG eg) {
         
         Connection connection = DBConnection.getMsSQLConnection();
         PreparedStatement statement;
         Tabella_Corso ct = new Tabella_Corso();

         
         boolean thereiscorso = ( ct.findByPrimaryKey(eg.getIdCorso())!=null);
         if (thereiscorso == false){
             String message = "Corso non esistente";
             JOptionPane.showMessageDialog(new JFrame(), message, "Dialog",
             JOptionPane.ERROR_MESSAGE);
             new Exception("Corso non esiste");
          } 
        
         String insert = "update "+ tableName +" set ID_Corso=?, Data_I=?, Data_F=?, N_Giornate=?, Note_E=? where Cod_EG=?";
        
         try {
   
       	  statement = connection.prepareStatement(insert);
       	  statement.setInt   (1, eg.getIdCorso());
		  statement.setDate  (2, eg.getDataInizio());
		  statement.setDate  (3, eg.getDataFine());
		  statement.setInt   (4, eg.getNrGiornate());
		  statement.setString(5, eg.getNote());
		  statement.setInt(6, eg.getCodice_EG());

             statement.executeUpdate();
             
         } catch (SQLException e) {
             e.printStackTrace();
           }
         
         JOptionPane.showMessageDialog(null, "Edizione Giornata aggiornata");

     }
    
     public void delete(String cod_eg) {
         Connection connection = DBConnection.getMsSQLConnection();

         PreparedStatement statement = null;
         String insert = "delete from "+ tableName +" where Cod_EG = ?";
         try {
             statement = connection.prepareStatement(insert);
             statement.setString(1, cod_eg);
             statement.executeUpdate();
         }
         catch (SQLException e) {
                 new Exception(e.getMessage());
             System.out.println("Errore"+ e.getMessage());
         }
         finally {
             try {
                 if (statement != null) 
                     statement.close();
                 if (connection!= null)
                     connection.close();
             }
             catch (SQLException e) {
                 new Exception(e.getMessage());
                     System.out.println("Errore"+ e.getMessage());
             }
         }
     }
     
     ///////sto modificando il tipo string in int  della fun finprimr
     public EdizioneG findByPrimaryKey(int cod_eg)  {
        EdizioneG edizioneg = null;
         
         Connection connection = DBConnection.getMsSQLConnection();
         PreparedStatement statement = null;
         String query = "select * from "+ tableName +" where Cod_EG=? ";
         try {
             statement = connection.prepareStatement(query);
             // sto cambiando setString in setInt
             statement.setInt(1, cod_eg);
             ResultSet result = statement.executeQuery();
             if (result.next()) {
            	 edizioneg = new EdizioneG();
            	 edizioneg.setIdCorso(result.getInt("ID_Corso"));
            	 edizioneg.setDataInizio(result.getDate("Data_I"));
            	 edizioneg.setDataInizio(result.getDate("Data_F"));
            	 edizioneg.setNrGiornate(result.getInt("N_Giornate"));
            	 edizioneg.setNote(result.getString("Note_E"));
             }
            	
         }
         catch (SQLException e) {
                 new Exception(e.getMessage());
             System.out.println("Errore"+ e.getMessage());
         }
         finally {
             try {
                 if (statement != null) 
                     statement.close();
                 if (connection!= null)
                     connection.close();
             } catch (SQLException e) {
                 new Exception(e.getMessage());
                      System.out.println("Errore"+ e.getMessage());
             }
         }
         return edizioneg;
     }  
     
     
     
     /*
     public List<Utente> findAll()  {
         List<Utente> utenti = null;
         Utente utente = null;
         Connection connection = DBConnection.getMsSQLConnection();
         PreparedStatement statement = null;
         String query = "select * from "+ tableName ;
         try {
             statement = connection.prepareStatement(query);
             ResultSet result = statement.executeQuery();
             if(result.next()) {
                 utenti = new LinkedList<Utente>();
                 utente = new Utente();
                 utente.setCF(result.getString("CF"));
                 utente.setCognome(result.getString("Cognome"));
                 utente.setNome(result.getString("Nome"));
                 utente.setDataN(result.getDate("DataNascita"));
                 utente.setLuogoN(result.getString("LuogoNascita"));
                 utente.setVia(result.getString("Via"));
    			 utente.setNrCivico(result.getInt("Civico"));
    			 utente.setCitt�(result.getString("Citt�"));
    			 utente.setCAP(result.getInt("CAP"));
    			 utente.setEmail(result.getString("Email"));
    			 utente.setCellulare(result.getString("Cellullare"));
             }
             
             while(result.next()) {
            	 utente = new Utente();
            	 utente.setCF(result.getString("CF"));
            	 utente.setNome(result.getString("nome"));
            	 utente.setCognome(result.getString("cognome"));
            	 utente.setCitt�(result.getString("citt�"));
            	 utente.setVia(result.getString("via"));
            	 utente.setNrCivico(result.getInt("nrCivico"));
            	 utente.setCAP(result.getInt("CAP"));
            	 utenti.add(utente);
             }
         }
         catch (SQLException e) {
                 new Exception(e.getMessage());
             System.out.println("Errore"+ e.getMessage());
         }
         finally {
             try {
                 if (statement != null) 
                     statement.close();
                 if (connection!= null)
                     connection.close();
             } catch (SQLException e) {
                 new Exception(e.getMessage());
                      System.out.println("Errore"+ e.getMessage());
             }
         }
         return utenti;
     } 
     */

     
     
}


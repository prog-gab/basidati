package model;

public class DocenteEST extends Utente{
	private Integer P_Iva;
	
	public DocenteEST() {}

	public Integer getPiva() {
		return this.P_Iva;
	}
	
	public void setPiva(Integer pIva) {
		this.P_Iva = pIva;
	}
	
}